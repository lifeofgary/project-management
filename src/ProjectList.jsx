
import Button, { button_style } from "./components/Button"
import { useState } from "react"
import useLocalStorage from "use-local-storage"
import * as Dialog from '@radix-ui/react-dialog';
import initial_project_state from "./project_management/initial_project_state";
import deepcopy from "deepcopy";
import fileDownload from 'js-file-download'

const Project = ({name, onClick}) => {
    const [data] = useLocalStorage( locate_prefix(name), [])
    return (
        <li
            className="cursor-pointer hover:bg-gray-300 px-5"
            onClick={()=>onClick({...initial_project_state, ...data})}> {name} </li>
    )
}

const LoadButton = ({projects, loadProject, disabled, color})=>{
    const [open, setOpen] = useState(false);
    const onClick = name=>{
        loadProject(name)
        setOpen(false)
    }

    return (
        <Dialog.Root open={open}>
            <Dialog.Trigger >
                <div
                    className={button_style(color, {disabled: disabled, border: true})}
                    onClick={()=>setOpen(true)}
                >
                    Load
                </div>
            </Dialog.Trigger>
            <Dialog.Content>
                <div className="bg-gray-200 rounded-xl m-2 pt-3">
                    <ul >
                        {projects.map( name=><Project key={name} name={name} onClick={name=>onClick(name)} />)}
                    </ul>
                    <Dialog.Close>
                        <div
                            className={button_style("slate")+" m-4"}
                            onClick={()=>setOpen(false)}
                        >
                            Close
                        </div>
                    </Dialog.Close>
                </div>
            </Dialog.Content>
        </Dialog.Root>
    )
}

const local_index   =  "project_management"
const locate_prefix = name => `${local_index}_${name}`

const SaveButton =( {name, color,  state, disabled, local_storage, children}) => {
    const [ , save_to_local_storage] = useLocalStorage( locate_prefix(name), [])
    const [projects, save_projects] = local_storage


    const save_data = () =>{
        let new_state = deepcopy(state)
        let keys = Object.keys( new_state)

        // Remove all headers before saving
        for (let key of keys ) {
            if (new_state[key].headers)
                delete new_state[key].headers
        }

        save_to_local_storage(new_state)
        if (!projects.includes(name)) {
            let p = projects.concat(name)
            save_projects(p);
        }
    }

    return (
        <Button color={color} onClick={ save_data } disabled={disabled} border>
            {children}
        </Button>
    )
}

const ProjectList = ({state, set_state})=>{
    const [, setName] = useState('');
    const [projects, save_projects] = useLocalStorage( local_index, [])

    const loadProject = data => {
        let keys = Object.keys( initial_project_state )

        // Restore deleted headers
        for (let key of keys ) {
            if (initial_project_state[key].headers)
                data[key].headers = initial_project_state[key].headers
        }

        set_state(data)
        console.log("Load Data", data)
        // set_state( initial_project_state );
    }

    const new_project = ()=> {
        const new_name = prompt("New name");
        setName(new_name);
        let state = deepcopy(initial_project_state)
        state.name = new_name;
        set_state( state)
    }

    const export_project = async data => {
        fileDownload(JSON.stringify(data,0,2), `${data.name}.json`);
    }

    const import_project = (event) => {
        const reader = new FileReader();
        reader.readAsText( event.target.files[0] )
        reader.onload = () => {
            const state = JSON.parse( reader.result );
            set_state( state )
        }
    }

    return (
        <>
            <Button border color="zinc" onClick={new_project}>New</Button>
            <SaveButton
                color="zinc"
                name={ state===null ? '' :  state.name}
                state={state}
                disabled={state===null}
                local_storage = {[projects, save_projects]}
            >
                Save
            </SaveButton>
            <LoadButton projects={projects} loadProject={loadProject} color="zinc" disabled={projects.length===0} />
            <Button border color="zinc" disabled={state===null} onClick={()=>export_project(state)}>Export</Button>
            {/* <Button border color="zinc" onClick={new_project}>Import</Button> */}

            <label className={button_style("zinc", {border: true})}  htmlFor="file_input">Import</label>
            <label onChange={import_project} >
                <input
                    className="hidden file:text-zinc-500 file:rounded-xl file:py-2 file:px-4 file:m-1 file:hover:bg-zinc-500 file:hover:text-white file:border-zinc-500 file:border" 
                    id="file_input" type="file"
                />
            </label>
        </>
    )
}

/*
<button class="zinc rounded-xl py-2 px-4 m-1 text-base text-zinc-500 hover:bg-zinc-500 hover:text-white border-zinc-500 border"> Import </button>
*/

export default ProjectList
