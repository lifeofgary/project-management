
import { Link, Routes, Route, useLocation } from 'react-router-dom';
import * as Tabs from '@radix-ui/react-tabs';

import Colors from "./examples/Colors.jsx"
import Pictures from './examples/Pictures';
import Links from './examples/Links'
import Icons from "./examples/Icons"
import Drawings from "./examples/Drawings"
import Buttons from "./examples/Buttons"

import Videos from "./project_management/Videos"
import ProjectManagement from './project_management/ProjectManagement';
 
const PROJECT_MANAGEMENT = "/project-management/"
const VIDEO    ="/project-management/videos";
const COLORS   ="/colors";
const BUTTONS  ="/buttons";
const ICONS    ="/icons";
const DRAWING  ="/drawings";
const PICTURES ="/pictures";
const LINKS    ="/links";

const TabTrigger = ({to, children})=>{
    let location = useLocation();

    let inactive = "text-gray-400 bg-gray-200 hover:bg-gray-100 hover:border-gray-100 hover:text-zinc-400 rounded-t-xl mr-1 border"
    let active   = "text-zinc-500 bg-gray-100 border-gray-100 rounded-t-xl mr-1 border"

    let styles = location.pathname === to ? active : inactive;

    return (
        <Tabs.Trigger className={`${styles}  flex items-center justify-center px-5 py-2 text-xl text-gray-900 duration-300 cursor-pointer;`}>
            <Link to={to}>{children}</Link> {' '}
        </Tabs.Trigger>
    )
}

const Tab = props=>{
    return (
        <>
            <Tabs.Root className="flex flex-col w-full bg-white shadow-sm" defaultValue="project_management" orientation="vertical">
                <Tabs.List className="flex" aria-label="tabs">
                    <TabTrigger to={PROJECT_MANAGEMENT}> Project Management </TabTrigger>
                    <TabTrigger to={VIDEO}> Videos </TabTrigger>
                    {/* <TabTrigger to={COLORS}> Colors </TabTrigger> */}
                    {/* <TabTrigger to={BUTTONS}> Buttons </TabTrigger> */}
                    {/* <TabTrigger to={ICONS}> Icons </TabTrigger> */}
                    {/* <TabTrigger to={DRAWING}> Drawings </TabTrigger> */}
                    {/* <TabTrigger to={PICTURES}> Pictures </TabTrigger> */}
                    {/* <TabTrigger to={LINKS}> Links </TabTrigger> */}
                </Tabs.List>
                
                <div className="mr-1 p-5 duration-700 bg-gray-100 outline-none rounded-b-md focus:shadow-xl">
                    <Routes>
                        <Route path={VIDEO} element={<Videos />} />
                        <Route path={COLORS} element={<Colors />} />
                        <Route path={BUTTONS} element={<Buttons />} />
                        <Route path={ICONS} element={<Icons />} />
                        <Route path={DRAWING} element={<Drawings />} />
                        <Route path={PICTURES} element={<Pictures />} />
                        <Route path={LINKS} element={<Links />} />
                        <Route path={`/project-management/*`} element={<ProjectManagement {...props} />} />

                    </Routes>
                </div>
            </Tabs.Root>
        </>
    )
}

export default Tab
