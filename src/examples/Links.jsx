
import { H2, H3 } from "../components/Components"

const Link = ({href, children})=>{
    return (
        <li className="ml-8 text-green-700">
            <a href={href}> {children} </a>
        </li>
    )
}

const Links = ()=>{
    return (
        <div>
            <H2>Webdesign Links</H2>

            <H3>Design Modo</H3>

            <ul className="list-decimal">
                <Link href="https://designmodo.com/website-design/">Website Design</Link>
                <Link href="https://designmodo.com/hero-headers/">Hero Headers</Link>
                <Link href="https://designmodo.com/design-type-photo/">Desgin Photos</Link>
                <Link href="https://designmodo.com/parallax-scrolling-web-design/">Parallax Scrolling</Link>
                <Link href="https://designmodo.com/photo-backgrounds-web-design/">Photo Backgrounds</Link>
                <Link href="https://designmodo.com/create-featured-images/">Creating Featured Images</Link>
                <Link href="https://designmodo.com/css-graph-chart-tutorials/">Chart Tutorials</Link>
                <Link href="https://designmodo.com/color-psychology-web-design/">Color Psychology</Link>
                <Link href="https://designmodo.com/free-illustrations/">Free Illustrations</Link>
                <Link href="https://designmodo.com/free-essentials-designers/">Designer Essentails</Link>
                <Link href="https://designmodo.com/web-design-people-illustrations/">Illustrations</Link>
                <Link href="https://designmodo.com/responsive-design-examples/">Responsive Design</Link>
                <Link href="https://designmodo.com/scrapbook-website-design/">Scapbook Web Design</Link>
                <Link href="https://designmodo.com/masculine-feminine-designs/">Masculine Feminine Design</Link>
            </ul>

            <H3 >Misc</H3>
            <ul className="list-decimal">
                <Link href="https://flatuicolors.com">Color Picker</Link>
                <Link href="https://designmodo.com/free-illustrations/">Free Illustrations</Link>
                <Link href="https://www.npmjs.com/package/react-albus">Albus: Headless Wizard</Link>
                <Link href="https://blog.logrocket.com/the-complete-guide-to-building-headless-interface-components-in-react/">Headless guide</Link>
                <Link href="https://github.com/jxom/awesome-react-headless-components">Awesome Headless Components</Link>
                <Link href="https://github.com/bjoernWahle/react-tree-select-hook">React Tree Select</Link>
                <Link href="https://github.com/rehooks/awesome-react-hooks">Awesome React Hooks 1</Link>
                <Link href="https://github.com/glauberfc/awesome-react-hooks">Awesome React Hooks 2</Link>
            </ul>

            <H3>Radix</H3>
            <ul className="list-decimal">
                <Link href="https://github.com/ecklf/tailwindcss-radix#readme">Radix</Link>
        	    <Link href="https://www.radix-ui.com/docs/primitives/overview/getting-started">Radix:Getting Started</Link>
                <Link href="https://tailwindcss-radix.vercel.app">Example 1</Link>
                <Link href="https://codesandbox.io/s/xrxw8">Example 2</Link>
                <Link href="https://tailwindcss-radix.vercel.app">Example: Rewinds</Link>
            </ul>

            <H3>Refactoring UI</H3>
	
	
        </div>
    )
}

export default Links
