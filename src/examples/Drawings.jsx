import { useState } from "react"
import {table} from "../Classes"

const Icons = ()=>{
    const filters = [
        "no-filter",
        "blur-sm",
        "brightness-50",
        "contrast-150",
        "grayscale",
        "invert",
        "hue-rotate-30",
        "hue-rotate-60",
        "hue-rotate-90",
        "hue-rotate-180",
        "-hue-rotate-30",
        "-hue-rotate-60",
        "-hue-rotate-90",
        "-hue-rotate-180",
        "invert hue-rotate-30",
        "invert hue-rotate-60",
        "invert hue-rotate-90",
        "invert hue-rotate-180",
        "invert -hue-rotate-30",
        "invert -hue-rotate-60",
        "invert -hue-rotate-90",
        "invert -hue-rotate-180",
        "sepia",
    ]

    const [index, set_index] = useState(0)
    const filter = filters[index]
    const list = filters.map( (f,i)=>(
        <span key={f} onClick={ ()=>set_index(i)} className={`mx-2 ${index===i ? "font-bold" : "" }`}>{f}</span>

    )) 

    const i = icons.map( image => (
        <div key={image} className="pb-5">
            <p className="font-semibold">{image}</p>
            <img src={`/project-management/icons/${image}.svg`} className={`${filter} max-w-full h-auto`} alt=""/>
        </div>
    ))
    
    return (
        <>
            {list}
            <div className={table.grid(5)}>   
                {i}    
            </div>
        </>
    )
}

export default Icons

const icons = [
    ["android"],
    ["android1"],
    ["app-store"],
    ["arrow"],
    ["art"],
    ["bag"],
    ["basket"],
    ["book"],
    ["bowling"],
    ["box"],
    ["brush"],
    ["building"],
    ["bulb"],
    ["button"],
    ["calculator"],
    ["calendar"],
    ["camera"],
    ["car"],
    ["card"],
    ["chair"],
    ["chat"],
    ["clipboard"],
    ["clocks"],
    ["compas"],
    ["converse"],
    ["cup"],
    ["dj"],
    ["donut"],
    ["dude"],
    ["dynamite"],
    ["earth"],
    ["egg"],
    ["eye"],
    ["file"],
    ["fit"],
    ["flag"],
    ["flask"],
    ["flower"],
    ["games"],
    ["gift-box"],
    ["girl"],
    ["goal"],
    ["google"],
    ["graph"],
    ["icecream"],
    ["icons"],
    ["imac"],
    ["ipad"],
    ["iphone"],
    ["key"],
    ["lettersymbol"],
    ["lock"],
    ["loop"],
    ["macbook"],
    ["magic"],
    ["magicmouse"],
    ["mail"],
    ["map"],
    ["medal"],
    ["mic"],
    ["money"],
    ["mortarboard"],
    ["mountain"],
    ["news"],
    ["paper-bag"],
    ["pc"],
    ["pencil"],
    ["pencils"],
    ["picture"],
    ["pig"],
    ["pills"],
    ["play"],
    ["printer"],
    ["responsive"],
    ["retina"],
    // ["ribbon"],
    ["ring"],
    ["rocket"],
    ["rss"],
    ["safe"],
    ["save"],
    ["search"],
    ["settings"],
    ["shield"],
    ["shirt"],
    ["skateboard"],
    ["spray"],
    ["support"],
    ["ticket"],
    ["toilet-paper"],
    ["touch"],
    ["trash"],
    ["trip-bag"],
    ["truck"],
    ["ubmrella"],
    ["user-interface"],
    ["video"],
    ["weather"],
    ["wi-fi"],
    ["wine"],
    ["yinyang"],
]