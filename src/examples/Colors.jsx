
import { H2, H3 } from "../components/Components"

const Swatch = ({name}) => {

    if (name==="skip") return <div />

    let text_color = name.split("-")[0];

    return (
        <div className="">
            <div className={`h-20 w-full bg-${name} `} >
                <div className={`pl-4 pt-4 font-medium text-${text_color}-900`}>{name}</div>
                {/* <div className={`pl-4 text-${text_color}-900 font-mono lowercase`}>{color}</div> */}
            </div>
        </div>
    )
}

const tailwind = [
    {name: "teal-500"},
    {name: "green-500"},
    {name: "cyan-500"},
    {name: "purple-500"},
    {name: "slate-600"},

    {name: "teal-600"},
    {name: "green-600"},
    {name: "cyan-600"},
    {name: "purple-700"},
    {name: "slate-700" },

    {name: "amber-400"},
    {name: "orange-500"},
    {name: "red-500"},
    {name: "gray-200"},
    {name: "slate-400" },

    {name: "amber-500"},
    {name: "orange-600"},
    {name: "red-600"},
    {name: "gray-300"},
    {name: "slate-500"},
]

const full_pallet = [
    {name: "slate-500"}, 
    {name: "gray-500"},
    {name: "zinc-500"},
    {name: "stone-500"},
    {name: "skip"},

    {name: "slate-600"},
    {name: "gray-600"},
    {name: "zinc-600"},
    {name: "stone-600"},
    {name: "skip"},

    {name: "red-500"},
    {name: "orange-500"},
    {name: "amber-500"},
    {name: "yellow-500"},
    {name: "lime-500"},

    {name: "red-600"},
    {name: "orange-600"},
    {name: "amber-600"},
    {name: "yellow-600"},
    {name: "lime-600"},

    {name: "green-500"},
    {name: "emerald-500"},
    {name: "teal-500"},
    {name: "cyan-500"},
    {name: "sky-500"},

    {name: "green-600"},
    {name: "emerald-600"},
    {name: "teal-600"},
    {name: "cyan-600"},
    {name: "sky-600"},


    {name: "blue-500"},
    {name: "indigo-500"},
    {name: "violet-500"},
    {name: "purple-500"},
    {name: "fuchsia-500"},

    {name: "blue-600"},
    {name: "indigo-600"},
    {name: "violet-600"},
    {name: "purple-600"},
    {name: "fuchsia-600"},


    {name: "pink-500"},
    {name: "rose-500"},
    {name: "red-500"},
    {name: "skip"},
    {name: "skip"},

    {name: "pink-600"},
    {name: "rose-600"},
    {name: "red-600"},
    {name: "skip"},
    {name: "skip"},

]

const SwatchGroup = ({array, min, max})=>{
    return (
        <div className="mb-8 min-w-0 flex-1 grid grid-cols-5 gap-x-4">
            { array.filter( (_,index)=> min <= index && index<max ).map( (color, index)=><Swatch {...color} key={index} />)}
        </div>
    )
}

const Colors = ()=>{
    const colors = tailwind ;

    // :link :visited :hover :active
    return (
        <>        
            <H2> Colors</H2>

            <H3>From <a href="https://designmodo.github.io/Flat-UI/">Design Modo</a></H3>

            <SwatchGroup array={colors} min="0" max="10" />
            <SwatchGroup array={colors} min="10" max="20" />

            <H3>Tailwind Colors</H3>

            <SwatchGroup array={full_pallet} min="0" max="10" />
            <SwatchGroup array={full_pallet} min="10" max="20" />
            <SwatchGroup array={full_pallet} min="20" max="30" />
            <SwatchGroup array={full_pallet} min="30" max="40" />
            <SwatchGroup array={full_pallet} min="40" max="50" />

        </>
    )
}
   
export default Colors