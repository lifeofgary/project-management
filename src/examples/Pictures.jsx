
import { H2 } from "../components/Components"
import AnchorToTab from "../components/AnchorToTab"

const PictureLink = ({href, title, image, children, ...props})=>{
    return (
        <div {...props}>
            <H2><AnchorToTab href={href}>{title}</AnchorToTab></H2>
            <p className="mb-4">
                <img src={`/project-management/images/${image}.jpg`} className={`max-w-full h-auto`} alt=""/>
            </p>
            
        </div>
    )
}

const Pictures = () => {
    return (
        <div>
            <p>
                From <AnchorToTab href="https://designmodo.com/free-photos/">Design Modo</AnchorToTab>
            </p>

            <PictureLink href="https://freephotos.cc/en" title="FreePhotos.cc" image="freephotos-cc">
                Creative commons photos.
            </PictureLink>

            <PictureLink href="https://wdrfree.com/webdesignresources/73/raumrot" title="Unsplash" image="unsplash">
                Handpicked images.
            </PictureLink>

            <PictureLink href="https://littlevisuals.co/" title="Little Visuals" image="Little-Visuals">
                Detail pattern images.
            </PictureLink>

            <PictureLink href="https://gratisography.com/" title="Gratisography" image="Gratisography">
                Photos by Ryan McGuire of Belle Design.
            </PictureLink>

            <PictureLink href="https://getrefe.tumblr.com/" title="Free Refe Mobile Photos" image="Free-Refe-Mobile-Photos">
                Photos taken by mobile phones.
            </PictureLink>

            <PictureLink href="https://jaymantri.com/" title="Jay Mantri" image="Jay-Mantri">
                Photos by Jay Mantri.
            </PictureLink>

            <PictureLink href="https://mystock.themeisle.com/" title="mystock.photos" image="mystock">
                Curated list of phones
            </PictureLink>

            <PictureLink href="https://magdeleine.co/" title="Magdeleine" image="Magdeleine">
                Categorized hand-picked images.
            </PictureLink>
            
            <PictureLink href="https://www.foodiesfeed.com/" title="Foodie’s Feed" image="Foodies-Feed">
                Pictures of foood.
            </PictureLink>
            
            <PictureLink href="https://picography.co/" title="Picography" image="Picography">
                Face and event photography.
            </PictureLink>

            <PictureLink href="https://nos.twnsnd.co/" title="New Old Stock" image="New-Old-Stock">
                Old photos.
            </PictureLink>

            <PictureLink href="https://superfamous.com/" title="SuperFamous" image="SuperFamous">
                Photos by Dutch interaction designer Folkert Gorter.
            </PictureLink>

            <PictureLink href="https://www.publicdomainarchive.com" title="Public Domain Archive" image="Public-Domain-Archive">
                Old photos.
            </PictureLink>

            <PictureLink href="https://thepatternlibrary.com/" title="Amazing Pattern Library" image="The-Amazing-Pattern-Library">
                Patterns for backgrounds.
            </PictureLink>


        </div>
    )
}

export default Pictures