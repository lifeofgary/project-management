
import Button from "../components/Button"
import { H2, H3 } from "../components/Components"

const Buttons = () => {
    return (
        <>
            < H2 > Buttons </H2>

            <H3>Normal</H3>
            <div>
                <Button teal > Teal </Button>
                <Button green> Green </Button>
                <Button cyan> Cyan </Button>
                <Button purple> Purple </Button>
                <Button dark_gray> Dark Gray </Button>
            </div>
            <div>
                <Button amber> Amber </Button>
                <Button orange> Orange </Button>
                <Button red> Red </Button>
                <Button gray> Gray </Button>
                <Button lite_gray> Lite Gray </Button>
            </div>

            <H3> Disabled </H3>
            <div>
                <Button teal disabled> Teal </Button>
                <Button green disabled> Green </Button>
                <Button cyan  disabled> Cyan </Button>
                <Button purple disabled> Purple </Button>
                <Button dark_gray disabled> Dark Gray </Button>
            </div>
            <div>
                <Button amber disabled> Amber </Button>
                <Button orange disabled> Orange </Button>
                <Button red disabled> Red </Button>
                <Button gray disabled> Gray </Button>
                <Button lite_gray disabled> Lite Gray </Button>
            </div>

            <H3> Transparent </H3>
            <div>
                <Button teal transparent> Teal </Button>
                <Button green transparent> Green </Button>
                <Button cyan  transparent> Cyan </Button>
                <Button purple transparent > Purple </Button>
                <Button dark_gray weight="700" transparent> Dark Gray </Button>
            </div>
            <div>
                <Button amber transparent> Amber </Button>
                <Button orange transparent> Orange </Button>
                <Button red transparent> Red </Button>
                <Button gray transparent> Gray </Button>
                <Button lite_gray transparent> Lite Gray </Button>
            </div>

            <H3> Transparent/Disabled </H3>
            <div>
                <Button teal transparent disabled> Teal </Button>
                <Button green transparent disabled> Green </Button>
                <Button cyan transparent disabled> Cyan </Button>
                <Button purple transparent disabled> Purple </Button>
                <Button dark_gray weight="700" transparent disabled> Dark Gray </Button>
            </div>
            <div>
                <Button amber transparent disabled> Amber </Button>
                <Button orange transparent disabled> Orange </Button>
                <Button red transparent disabled> Red </Button>
                <Button gray transparent disabled> Gray </Button>
                <Button lite_gray transparent disabled> Lite Gray </Button>
            </div>

            <H3> Border </H3>
            <div>
                <Button teal border> Teal </Button>
                <Button green border> Green </Button>
                <Button cyan border> Cyan </Button>
                <Button purple border> Purple </Button>
                <Button dark_gray border > Dark Gray </Button>
            </div>
            <div>
            <Button amber border> Amber </Button>
            <Button orange border> Orange </Button>
            <Button red border> Red </Button>
            <Button gray border> Gray </Button>
            <Button lite_gray border> Lite Gray </Button>
            </div>

            <H3> Border/Disabled </H3>
            <div>
                <Button teal border disabled> Teal </Button>
                <Button green border disabled> Green </Button>
                <Button cyan border disabled> Cyan </Button>
                <Button purple border disabled> Purple </Button>
                <Button dark_gray border disabled> Dark Gray </Button>
            </div>
            <div>
                <Button amber border disabled> Amber </Button>
                <Button orange border disabled> Orange </Button>
                <Button red border disabled> Red </Button>
                <Button gray border disabled> Gray </Button>
                <Button lite_gray border disabled> Lite Gray </Button>
            </div>

            <H3>Shape</H3>
            <div>
                <Button>Normal</Button>
                <Button pill>Pill</Button>
                <Button rectangle>Rectangle</Button>
            </div>

            <H3>Size</H3>
            <div>
                <Button size="4xl">4Xl</Button>
                <Button size="3xl">3Xl</Button>
                <Button size="2xl">2Xl</Button>
                <Button size="xl">Xl</Button>
                <Button>Normal</Button>
                <Button size="sm">Sm</Button>
                <Button size="xs">Xs</Button>
            </div>
        </>
    )
}

export default Buttons
