
import { H4 } from "./components/Components"
import AnchorToTab from "./components/AnchorToTab"

const Video = ({children, href}) => {
    return (
        <Header> 
            <AnchorToTab className={`text-gray-500 hover:text-gray-700 visited:text-gray-400`} href={href} target="_blank"  rel="noopener noreferrer">{children}</AnchorToTab>
        </Header>
    )
}

export const Header = ({children})=>{
    return (
        <H4>{children}</H4>
    )
}

export const File = ({children, important, review, skip, normal})=>{
    const child = <a href={`docs/${children}`}> {children} </a>

    if (normal)
        return (
            <div className="pl-6 text-teal-400 hover:text-teal-700 visited:text-teal-400">
                {child}
            </div>
        )

    if (skip)
        return
    if (important)
        return (
            <div className="pl-6 text-red-400 hover:text-red-700 visited:text-red-400">
                {child}
            </div>
        )

    if (review)
        return (
            <div className="pl-6 text-amber-400 hover:text-amber-700 visited:text-amber-400">
                {child}
            </div>
        )

    return (
        <div className="pl-6 text-zinc-400 hover:text-zinc-700 visited:text-zinc-400">
            {child}
        </div>
    )
}

export const List = ({children})=>{
    return (
        <div className="pl-3">{children}</div>
    )
}

export const Email = ({children}) => <H4 className="pl-3">{ children }</H4>            

export default Video

