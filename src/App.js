
import Tabs from "./Tabs"
import { DragDropContext } from 'react-beautiful-dnd';
import { useReducer } from "react";
import deepcopy from "deepcopy";

// https://medium.com/litslink/react-dnd-in-examples-ce509b25839d
// https://medium.com/litslink/react-js-router-in-examples-93c778a37888
// https://artem-diashkin.medium.com/list/my-stories-eaab179e4a13

// https://medium.com/litslink/d3-basics-how-to-create-a-bar-chart-232b53e398ee
// https://medium.com/litslink/storybook-in-examples-beginners-guide-6179cf83e1b6
// https://medium.com/geekculture/getting-started-with-cypress-io-17697edd7c0f
// https://betterprogramming.pub/most-useful-storybook-addons-and-features-in-examples-f751ed8ffbc

const INIT_SCHEDULE = [[]]

export const drag_and_drop_reducer = (state, { type, value }) => {
    let new_state = deepcopy(state)

    switch (type) {
        case "new": {
            const {table, schedule} = value
            const to = schedule.flat();

            // deserialze the tree
            let from = walk_children(table, "1");
            from.shift();

            from = from.filter( f=>to.find( t=>t.index!==f.index))

            new_state.from = from
            new_state.schedule = schedule
            break;
        }
        case "drop": {
            const {
                source: { index: source_index, droppableId: source_id },
                destination: { /* index: dest_index, */ droppableId: dest_id } 
            } = value

            if (source_id === dest_id)
                break;

            let to_move
            // find item and remove it from list
            if (source_id === "from") {
                to_move = new_state.from[source_index]
                new_state.from = new_state.from.filter((item, i) => i !== source_index)
            }
            else {
                to_move = new_state.schedule[source_id][source_index]
                new_state.schedule[source_id] = new_state.schedule[source_id].filter((item, i) => i !== source_index)
            }

            // add moved item to list
            if (to_move) {
                if (dest_id === "new") {
                    // create an item at the same time
                    new_state.schedule = new_state.schedule.concat([[to_move]])
                    break;
                } else if (dest_id === 'from') {
                    new_state.from = new_state.from.concat(to_move)
                }
                else
                    // add a sequential item
                    new_state.schedule[+dest_id][new_state.schedule[+dest_id].length] = to_move
            }
            break;
        }
        default: break;
    }

    return new_state;
}

const walk_children = (data, index) => {
    let ret = [
        {
            index,
            text: data.text
        }
    ]

    for (let child_index = 0; child_index < data.children.length; child_index++) {
        ret = ret.concat(walk_children(data.children[child_index], `${index}.${child_index + 1}`))
    }

    return ret
}

function App() {
    const [drag_and_drop_state, drag_and_drop_dispatch] = useReducer(drag_and_drop_reducer, { from: [], new: [], "schedule": INIT_SCHEDULE })

    // Function called when Drag Ends
    const onDragEnd = (result) => {
        const { source, destination } = result
        if (!destination) {
            return;
        }

        if (!source)
            return

        drag_and_drop_dispatch({
            type: "drop",
            value: result,
        })
    }

    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <div className="container mx-auto py-12 px-6">
                <Tabs
                    drag_and_drop_state={drag_and_drop_state}
                    drag_and_drop_dispatch={drag_and_drop_dispatch}
                />
            </div>
        </DragDropContext>
    );
}
export default App;