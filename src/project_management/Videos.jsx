
import Video, {File, List, Email} from "../Video"
import { H2 } from "../components/Components"

/*
Initiating Part 1
    What are thet 10 key problems in project management?
    key interpersonal skills essential to project management
    twelve principles of project management


    1. The project manager’s triangle highlights _____________, ______________and ______________.
    2. There are a total of ______________ Process Groups and ______________ individual processes in the PMI Framework.
    3. Scope is achieved using a step-by-step approach referred to as ______________ ______________.
    4. List three categories in the PMI Talent Triangle:  ______________, ______________ and ______________ and ______________.
    5. Quality focuses on ______________ ______________ ______________ and ______________ ______________ ______________.
    6. The planning method used by PMI® is referred to as ______________.

1 Which three inputs can necessitate the need for a Project Charter?
    1. Business Case
    2. Benefits Mgt Plan
    3. Agreements
2 What are three things a Project Charter may gain a Project Manager?
    1. Authority
    2. Resources for Planning
    3. Sponsorship
3 Who can approve a Project Charter?
    Sponsor
4 What is the primary purpose of the Identify Stakeholders process?
    Stakeholder Register (Actual/Potential)
5 Who is the group interested or impacted by the project?
    Stakeholders
6 The Assumption Log is an output of which process?
    Project Charter
7 A Project Charter is not always necessary.  True/False?
    False


*/

const Videos = ()=> {
    return (
        <>
            <H2>Initiating</H2>
            <Video href="https://us02web.zoom.us/rec/share/OrnJ3G7Qq9HY4tUZ3pGCwfm0Pp3yjTI5Zy826ei4cf-GBkWTwQK1Aue4dcdLz2Q.rXDvJkTbKKMCojPJ">
                Topic: GRC Project Management Basics Day 1
            </Video>

            <Video href="https://us02web.zoom.us/rec/share/njRAj82ixOzv_wca4sdSPCnGHOwPBrwurHDD2R6hpDRuKoMTyppChb3f4qvOvcP4.5i7eSjgMJcTvTaOU">
                Topic: GRC Project Management Basics Day 2
            </Video>
            <List>
                <File normal>1. Part 2 PMI Framework.docx</File>
                <File normal>2. Part 2 PMI Lifecycle Attributes.docx</File>
                <File normal>3. Part 2 Initiating Process Group.docx</File>
                <File normal skip>4. Part 2 Skill Check 1 Initiating.docx</File>
                <File normal>5. Part 2 Planning Process Group.docx</File>
                <File normal skip>6. Part 2 Skill Check 2 Planning.docx</File>
                <File normal>7. Part 2 Executing Process Group.docx</File>
                <File normal>8. Part 2 Skill Check 3 Executing.docx</File>
                <File normal>9. Part 2 Monitor and Control Process Group.docx</File>
                <File normal skip>10. Part 2 Skill Check 4 - M and C.docx</File>
                <File normal>11. Part 2 Close the Project Process Group.docx</File>
                <File normal skip>12. Part 2 Skill Check 5 Closing.docx</File>
                <File normal skip>13. Part 2 Activity 1.docx</File>
                <File normal skip>14. Part 2 Retrospective</File>
            </List>

            <Video href="https://us02web.zoom.us/rec/share/jq5Wo-gpIB9Dw8hggM3EJxEXq4k0LmfdgriGf47LtsmkjKoWOT0E1XmzCN6teO1Q.DNnX_t1qQvsXPr6J">
                Topic: GRC Project Management Basics Day 3
            </Video>
            <List>
                <File normal>1. Part 3 A Portfolio Approach.docx</File>
                <File normal>2. Part 3 A Portfolio Approach Example.docx</File>
                <File normal>3. Part 3 Activity 1 Portfolio.docx</File>
                <File review>3a.  Portfolio Planning.xlsx</File>
                <File normal>4. Part 3 The PMO Fit</File>
                <File normal skip>5. Part 3 Activity 2 PMO.docx</File>
                <File normal review>6. Part 3 Benefits Mgmt Plan Example.xlsx</File>
                <File normal review>7. Part 3 Benefits Mgm Plan Template</File>
                <File normal skip>8. Part 3 Activity 3 Business Case Format 2021.docx</File>
                <File review>9. Part 3 Business Case Example 2021.docx</File>
                <File normal skip>10. Part 3 Skill Check.docx</File>
                <File normal skip>11. Part 3 Retrospective.docx</File>
            </List>

            <Video href="https://us02web.zoom.us/rec/share/DcjUNEO2HkarEt8wqqVzi2_joEwM3hP8xlQ0pWDUAKzrX3kR0Rfa7-PowaBJFywn.1gdJiKyACj2TiypS">
                Topic: Getting to Scope:  System for Value Delivery
            </Video>

            <Video href="https://us02web.zoom.us/rec/share/HCGbK7S0hGe3tk2POYqrXFFta8_yD5czdicwAto1SaWe3vT7QWiv69Odg1tMh6y6.IAB1-IJ1_OVepwSq">
                Topic: GRC Project Management Basics Day 4
            </Video>

            <Video href="https://us02web.zoom.us/rec/share/PecyBVxxlYlWMtoPWQBVJnvJy71ZOeDJ6SHwYwcDJMG3_or6zvGoTxZmy5KF_GwR.JgXpBkr_c7wbJ6K1">
                Topic: Getting to Scope: Initiating the Project
            </Video>
            <List>
                <File normal>1. Part 4 Initiating Process Group.docx</File>
                <File normal skip>2. Part 4 Activity 1 Project Charter Format.docx</File>
                <File normal skip>2a. Part 4 Activity 1 Project Charter Example.docx</File>
                <File normal skip>3. Part 4 Activity 1 Project Charter Template.docx</File>
                <File normal skip>4a. Part 4 Activity 2 Stakeholder Reg Example.xlsx</File>
                <File review>4b. Part 4 Activity 2 Stakeholder Reg Temp.xlsx</File>
                <File normal review>5. Part 4 Activity 3 Assump Log.xlsx</File>
                <File skip>6. Part 4 Retrospective.docx</File>
                <File skip>7. Part 4 Skill Check.docx</File>
            </List>

            <Video href="https://us02web.zoom.us/rec/share/lTNC0D8Jc7b044gj6QP51Bxp1PQpb_PqdhYAkE_HEBPQnnsZOTf33gw9t-s2KjVH.nTGuU9A2yhFydDlj">
                Topic: GRC Project Management Basics Day 5
            </Video>
            <List>
                <Email>email subject: GRC Project Management Essentails</Email>            
                <File normal>Jones Family Reunion Project Plan Example 2022.xlsxProject Planning and Scheduling PKBOK Key Referneces.docx</File>

                <Email>email subject: RE: GRC Project Management Essentails</Email>            
                <File normal>Part 1 Activity 3 Stakeholder Eng Plan.xlsx</File>
                <File normal>Part 1 Activity 3 Project Charter Cast Study 2021.docx</File>
            </List>

            <H2>Planning</H2>
            <Video href="https://us02web.zoom.us/rec/share/hwI3xpIcfVW08jZKHX_3vu8FaJg8Xxja5jG1yT9aEx6Yo90HJT_nrYLsyO66XoOQ.3Ir4b93iRZHvlkww">
                Topic: GRC Planning and Scheduling Day 1
            </Video>
            <List>
                <File normal>Jones Family Reunion Project Plan Example Apr 2021.xlsx</File>
            </List>

            <Video href="https://us02web.zoom.us/rec/share/KhXInEYz4iyu-bJlSRMW1BfsB6x9AGZ26jyTUbjBNkuFRuOnU35KyWHU3InErLo.Y5WyBCtWtbGSoI0U">
                Topic: Topic: GRC Planning and Scheduling Day 2
            </Video>
            <List>
                <File normal>1. Part 1 Planning Process Sequence 2021.docx</File>
                <File normal skip>2. Part 1 Activity 1.docx</File>
                <File normal skip>3. Part 1 Activity 2.docx</File>
                <File review>4. Part 1 Activity 3 Project Charter Case Study 2021.docx</File>
                <File review>5. Part 1 Activity 3 Stakeholder Eng Plan.xlsx</File>
                <File normal skip>5. Part 1 Skill Check.docx</File>
                <File normal skip>6. Part 1 Retrospective.docx</File>
            </List>

            <Video href="https://us02web.zoom.us/rec/share/jS31kmg7zZb1HQOsK7OxJwFqUTZHJDX0fZvlmxZGUo8Nx_IvUTp7FvG_PTA8WwxE.B4zY03acXpNQJ1-I">
                Topic: GRC Planning and Scheduling Day 1
            </Video>

            <Video href="https://us02web.zoom.us/rec/share/cI8-SV0M3qBQuTs8w_BWnartVjkyT-E9HHtlFOzwC_kqZ5iJ2dLDsyhZ7X2m9fae.YdVqEAquhUAA8fml">
                Topic: GRC Planning and Scheduling Day 3
            </Video>
            <List>
                <File normal skip>0. A note about the Network Diagram - number 11.txt</File>
                <File normal>1. Part 2 Project Scope Management 2021.docx</File>
                <File normal>2. Part 2 Requirement Types to Consider.docx</File>
                <File normal skip>3. Part 2 Activity 1 Catagorized Req.docx</File>
                <File reivew>4. Part 2 Activity 1 CTQ Tree.xlsx</File> */}
                <File normal skip> -6. Part 2 Activity 3 WBS.docx</File> */}
                <File review>7. Part 2 Activity 3 WBS Dictionary.docx</File>
                <File normal skip>8. Part 2 Scope Planning Skill Check.docx</File>
                <File important>9. Part 2 Project Sched Mgmt 2021.docx</File>
                <File important>10. Part 2 Network Diagramming Steps.docx</File>
                <File normal skip>11. Part 2 Activity 4 Network Diagram.docx</File>
                <File normal>12. Part 2 Schedule Planning Skill Check.docx</File>
                <File normal>13. Part 2 Project Cost Management 2021.docx</File>
                <File important>14. Part 2 Activity 5 RBS.xlsx</File>
                <File normal skip>15. Part 2 Retrospective.docx</File>
            </List>
            <List>
                <File normal>
                    Part 2 Activity 5 RBS Update.xlsx   
                </File>
            </List>

            <Video href="https://us02web.zoom.us/rec/share/MuGxpxAl_MDEG1uwZUohCsRvPYPUCvaZURL4g5BeIgS20GZTTMlE08gdevDck577.0Eagddoj2GtDHOJ6">
                Topic: GRC Planning and Scheduling Day 4
            </Video>
            <List>
                <File normal>1. Part 3 Project Quality Management Plan 2021.docx</File>
                <File important>2. Part 3 Activity 1 Quality Mgmt Plan.xlsx</File>
                <File normal>3. Part 3 Project Resource Management 2021.docx</File>
                <File important>4. Part 3 Activity 2 RAM Example.docx</File>
                <File important>5. Part 3 Activity 2 RAM Template.docx</File>
                <File important>6. Part 3 Activity 3 Team Charter Template.docx</File>
                <List normal>7. Part 3 Skill Check 1.docx</List>
                <File normal>8. Part 3 Communications Management Plan 2021.docx</File>
                <File important>9. Part 3 Activity 4 Comm Mgm Plan Example.docx</File>
                <File important>10. Part 3 Activity 4 Comm Mgm Plan Template.docx</File>
                <File important>11. Part 3 Activity 4 Comm Mgm Plan Plus Template.docx</File>
                <File important>12. Part 3 Activity 5 Project Status Report Template.docx</File>
                <File normal>13. Part 3 Project Risk Management 2021.docx</File>
                <File normal>13b. Part 3 Project Risk Management Example.docx</File>
                <File review>14. Part 3 Activity 6 Risk Management Plan.docx</File>
                <File review>15. Part 3 Activity 7 Risk Reg Template.xlsx</File>
                <List noraml skip>16. Part 3 Skill Check 2.docx</List>
                <File normal>17. Part 3 Project Procurement Management 2021.docx</File>
                <List normal skip>18. Part 3 Activity 8 Procurement Analysis.docx</List>
                <List normal skip>18. Part 3 Retrospective.docx</List>
            </List>

            <Video href="https://us02web.zoom.us/rec/share/KzncRsNOEhUEVaq610OFLxbesigdHpaIcRhPuy5pS2tzjA_i2lPC4LcHbDEfITOG.XQZWrYoVqY4i5VGD">
                Topic: GRC Planning and Scheduling Day 5
            </Video>
            <List>
                <File normal>Communications Management Plan.xlsx </File>
                <File normal>Risk Management Example.docx</File>
            </List>

            <Video href="https://us02web.zoom.us/rec/share/1xb1WWWqJhn675ZcDBjv7VDX0O2ZbAmSL89ENLDhPNZ8HJXIEAcmWQy3X6dPXpJZ.ZeOEXq35ZqCjUYxw">
                Topic: GRC Planning and Scheduling Day 6
            </Video>
            <List>
                <File normal>1. Part 4 Project Management Plan Approval 2021.docx</File>
                <File normal skip>2. Part 4 Activity 1 Project Pitch.docx</File>
                <File normal skip>3. Part 4 Activity 2 Planning Events.docx</File>
                <File normal skip>4. Part 4 Skill Check.docx</File>
                <File normal skip>5. Part 4 Retrospective.docx</File>
            </List>

            <Video href="https://us02web.zoom.us/rec/share/-grug7IK7x9guBhquRrKo-eEiHRKnr6Md5QpVLXnWCVjaFZWcSzgMX3EPKPkS5aF.AXtuY11sumgMSEg9">
                Topic: GRC Planning and Scheduling Day 7
            </Video>

            <H2>Closing</H2>

            <Video href="https://us02web.zoom.us/rec/share/4Gm6sme0uKqNXBPU-T01FOoWT-IraRH6yQWFpPhp5R4q2pjdPsK04LdsCeMK0r3T.IXgMeR3xxZVInRx-">
                Topic: GRC Executing to Closing Class 1
            </Video>

            <Video href="https://us02web.zoom.us/rec/share/HJQCUhQ4uFv-NtoQM-1DqEJm7LZbGaVd-boMYpJ51KkD8kQhESlpx2VY95A7VNEQ.YFmFvTzkfiuLQyto">
                Topic: GRC Executing to Closing Class 2
            </Video>

            <Video href="https://us02web.zoom.us/rec/share/HJQCUhQ4uFv-NtoQM-1DqEJm7LZbGaVd-boMYpJ51KkD8kQhESlpx2VY95A7VNEQ.YFmFvTzkfiuLQyto">
                Topic: GRC Executing to Closing Class 2
            </Video>
            <List>
                <File normal>Cause and Effect Analysis Worksheet.xlsx</File>
                <File normal>Change Process Template.docx</File>
            </List>

            <Video href="https://us02web.zoom.us/rec/share/uhJagwuRKG9mbjbCp6fInacsK6z6fKBcEW9ynmKoHpY040_IYbVPopzCE8VsiJ7D.0tuQTvmM-eIHyYqo">
                Topic: GRC Executing to Closing Class 3
            </Video>

            <Video href="https://us02web.zoom.us/rec/share/_8oIbZNop9p1IiISPKVcZ1ZOksP0PpfOlsqZJajOz77kLHL8tcYX35pipgrB7dM-.W1S5Vhnox0SqgUbQ">
                Topic: GRC Executing to Closing Class 4
            </Video>
        </>
    )
}

export default Videos