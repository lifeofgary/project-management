import TableForProject from "./TableForProject";
import {H3} from "../components/Components"
import { 
    EDIT_CHARTER,
  } from "./reducer";

import ExampleButton from "../components/ExampleButton"
import Example from "./examples/ProjectCharterExample"
  
function ProjectCharter({...props}) {
    return (
        <div>
            <div className="overflow-x-auto">
                <H3>Project Charter</H3>
                <TableForProject  
                    {...props} 
                    on_edit_action={EDIT_CHARTER} 
                />
            </div>

            <ExampleButton>
                <Example />
            </ExampleButton>

        </div>
)}

export default ProjectCharter;
