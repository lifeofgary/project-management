
import { H3 } from '../components/Components'
import TableForProject from "./TableForProject"
import { 
    EDIT_COMMUNICATION,
    ADD_COMMUNICATION,
    EDIT_COMMUNICATION_ITEM,
    ADD_COMMUNICATION_ITEM,
} from "./reducer";
import Example from "./examples/CommunicationExample"
import ExampleButton from "../components/ExampleButton"

const Communication = ({dispatch, table, items}) => {
    return (
        <>
            <H3>Communication Management</H3>
            <p className="text-gray-400">
                Slide 108 from section 2.  Page 134 of <i>Project Management</i>
            </p>

            <TableForProject
                table={table}
                dispatch={dispatch}
                on_edit_action={EDIT_COMMUNICATION}
                on_add_action={ADD_COMMUNICATION}            
            />

            <H3>Communication Items</H3>
            <TableForProject
                table={items}
                dispatch={dispatch}
                on_edit_action={EDIT_COMMUNICATION_ITEM}
                on_add_action={ADD_COMMUNICATION_ITEM}
            />

            <ExampleButton>
                <Example />
            </ExampleButton>
        </>
    )
}

export default Communication