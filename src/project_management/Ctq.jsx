
import EditibleCell from "../components/EditibleCell"
import { EDIT_CTQ, ADD_CTQ } from "./reducer"
import { H4 } from "../components/Components"
import Example from "./examples/CtqExample"
import ExampleButton from "../components/ExampleButton"
import Button from "../components/Button"

let Ctq = ({dispatch, data}) => {
    const cell_style = weight => `p-4 text-gray-500 bg-amber-${weight} border mx-2 my-1 rounded border-zinc-700`
    const row_count = data[1].length;
    const top_rows = Math.floor((row_count-1)/2);
    const bottom_rows = row_count-1-top_rows;

    const styles = ""

    let phases = new Array(row_count).fill(1).map( (_, index) =>(
        <div key={index} className={cell_style(200)}>
            {`Key Phase ${index+1}`}
            <EditibleCell
                styles={styles}
                key={`1-${index}`}
                text={data[1][index]}
                // dispatch={dispatch}
                // on_edit_action={EDIT_CTQ}
                // row_index={index}
                // column_index={1}
                onEdit={value=>{
                    dispatch({
                        type: {EDIT_CTQ},
                        value,
                        row: {index},
                        column: 1
                    })
                }}
            />
        </div>
    )) 
    let deliverables = new Array(row_count).fill(1).map( (_, index) => (
        <div key={index} className={cell_style(300)}>
            {`Measureable Deliverable ${index+1}`}
            <EditibleCell
                styles={styles}
                key={`2-${index}`}
                text={data[2][index]}
                // dispatch={dispatch}
                // on_edit_action={EDIT_CTQ}
                // row_index={index}
                // column_index={2}
                onEdit={value=>{
                    dispatch({
                        type: {EDIT_CTQ},
                        value,
                        row: {index},
                        column: 2
                    })
                }}
            />
        </div>
    ))

    return (
        <div>
            <H4>Critical to Quality (CTQ) Tree</H4>
            <p className="text-gray-400">
                Slide 40 from section 2.
            </p>

            <div className={`grid grid-rows-${row_count} grid-flow-col min-w-fill`}>
                { row_count <= 1 ? "" : <div className={`row-span-${top_rows} `}></div>} 
                <div className={cell_style(100) }>
                    Project Goal
                    <div>
                        <EditibleCell
                            styles={styles}
                            key={"ctq"}
                            text={data[0][0]}
                            // dispatch={dispatch}
                            // on_edit_action={EDIT_CTQ}
                            // row_index={0}
                            // column_index={0}
                            onEdit={value=>{
                                dispatch({
                                    type: {EDIT_CTQ},
                                    value,
                                    row: 0,
                                    column: 0
                                })
                            }}
                        />
                    </div>
                </div>
                { row_count <= 2 ? "" : <div className={`row-span-${bottom_rows} `}></div>}

                { phases }
                { deliverables }

            </div> 

            <Button size="xs" onClick={()=>dispatch({type: ADD_CTQ})}> + </Button>

            <ExampleButton>
                <Example />
            </ExampleButton>

        </div>

    )
}

export default Ctq