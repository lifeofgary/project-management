
import { H3 } from '../components/Components'
import TableForProject from "./TableForProject"
import { 
    EDIT_RAM,
    ADD_RAM,
  } from "./reducer";

import { legend } from "../Classes"

const Ram = props=>{
    return (
        <>
            <H3>Responsibility Assignment Matrix</H3>
            <p className="text-gray-400">
                Slide 96 from section 2.  Page 116 of <i>Project Management</i>
            </p>

            <TableForProject {...props} 
                on_edit_action={EDIT_RAM}
                on_add_action={ADD_RAM}            
            />

            <div className={legend.title}>Legend</div>
            <div className={legend.grid}>
                <div className={legend.header}>Name</div>
                <div className={legend.header}>Definition</div>

                <div className={legend.odd_row}>A</div>
                <div className={legend.odd_row}>Approval Rquired</div>

                <div className={legend.even_row}>E</div>
                <div className={legend.even_row}>Execute</div>

                <div className={legend.odd_row}>C</div>
                <div className={legend.odd_row}>Consult With</div>
            </div>

        </>
    )
}

export default Ram