
import { H3 } from "../components/Components"
import TableForProject from "./TableForProject"
import { 
    EDIT_ASSUMPTION_LOG,
    ADD_ASSUMPTION_LOG,
  } from "./reducer.js";
  
import { legend } from "../Classes"

let header_class = legend.header;
let odd_row  = legend.odd_row;
let even_row = legend.even_row

const AssumptionLog = props=>{
    return (
        <>
            <H3>Assumption Log</H3>

            <TableForProject
                {...props} 
                on_edit_action={EDIT_ASSUMPTION_LOG}
                on_add_action={ADD_ASSUMPTION_LOG}
                    />

            <div className={legend.title}>Legend</div>

            <div className={legend.grid}>
                <div className={header_class}>Name</div>
                <div className={header_class}>Definition</div>

                <div className={odd_row}>Number</div>
                <div className={odd_row}>Assign a unique number to each assumption</div>

                <div className={even_row}>Category</div>
                <div className={even_row}>Category:  Assumption or Constraint</div>

                <div className={odd_row}>Risk Register</div>
                <div className={odd_row}>Enter "Y" or "N". Assumptions and constraints should be entered into the Risk Registe</div>

                <div className={even_row}>Description</div>
                <div className={even_row}>Provide brief description of assumption or constraint</div>

                <div className={odd_row}>Owner</div>
                <div className={odd_row}>Assign owner to monitor constraints and validate assumptions</div>

                <div className={even_row}>Status</div>
                <div className={even_row}>Provide current status of monitoring and validation effforts</div>

            </div>

        </>
)}

export default AssumptionLog