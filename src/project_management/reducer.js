import * as deepcopy from 'deepcopy';
import { find_child_in_wbs } from './helpers';

export const EDIT_CHARTER = Symbol('Edit Charter')
export const EDIT_STAKEHOLDER_REGISTRY = Symbol('Edit Stakeholder')
export const ADD_STAKEHOLDER_REGISTRY = Symbol('Add Stakeholder')
export const EDIT_ASSUMPTION_LOG = Symbol("Edit Assumption Log")
export const ADD_ASSUMPTION_LOG = Symbol("Add Assumption Log")
export const EDIT_STAKEHOLDER_ENGAGEMENT = Symbol("Edit Stakeholder Engagement")
export const ADD_STAKEHOLDER_ENGAGEMENT = Symbol("Add Stakeholder Engagement")
export const EDIT_RESOURCE_BREAKDOWN = Symbol("Edit Resource Breakdown")
export const ADD_RESOURCE_BREAKDOWN = Symbol("Add Resource Breakdown")
export const EDIT_QUALITY_OBJECTIVES = Symbol("Edit Quality Objectives")
export const ADD_QUALITY_OBJECTIVES = Symbol("Add Quality Objectives")
export const EDIT_DELIVERABLE_INFORMATION = Symbol("Edit Deliverable Information")
export const ADD_DELIVERABLE_INFORMATION = Symbol("Add Deliverable Information")
export const EDIT_RESOURCE_MANAGEMENT = Symbol("Edit Resource Management")
export const ADD_RESOURCE_MANAGEMENT = Symbol("Add Resource Management")
export const EDIT_RAM = Symbol("Edit RAM")
export const ADD_RAM = Symbol("Add RAM")
export const EDIT_COMMUNICATION = Symbol("Edit Communication")
export const ADD_COMMUNICATION = Symbol("Add Communication")
export const EDIT_COMMUNICATION_ITEM = Symbol("Edit Communication Item")
export const ADD_COMMUNICATION_ITEM = Symbol("Add Communication Item")
export const EDIT_RISK = Symbol("Edit RISK")
export const ADD_RISK = Symbol("Add RISK")
export const SET_STATE = Symbol("Set State")
export const EDIT_CTQ = Symbol("Edit CTQ")
export const ADD_CTQ = Symbol("Add CTQ")
export const EDIT_WBS = Symbol("Edit WBS")
export const ADD_WBS = Symbol("Add WBS")
export const EDIT_WBS_DICTIONARY = Symbol("Edit WBS dictionary")
export const ADD_WBS_DICTIONARY = Symbol("Add WBS dictionary")
export const EDIT_COST_ESTIMATE = Symbol("Edit Cost Estimate")
export const ADD_COST_ESTIMATE = Symbol("Add Cost Estimate")
export const EDIT_BUDGET = Symbol("Edit Budget")
export const ADD_BUDGET = Symbol("Add Budget")
export const SCHEDULE = Symbol("Schedule")

const reducer = (state, action) => {
    let new_state = deepcopy(state);

    const add_column = (key, options={}) => {
        const number_of_columns = new_state[key]['headers'].length
        const column = new Array(number_of_columns)
        column.fill('')
        if (options.init)
            for (const [key, value] of Object.entries(options.init))
                column[key] = value
        
        new_state[key]['body'].push(column)
    }

    const edit_column = key =>{
        new_state[key]['body'][action.row][action.column] = action.value;
    }

    switch (action.type) {
        case SCHEDULE: {
            new_state.schedule = action.value
            break;
        }
        case SET_STATE: {
            new_state = deepcopy(action.value)
            break;
        }
        case EDIT_CHARTER: {
            edit_column('charter');
            break;
        }
        case EDIT_STAKEHOLDER_REGISTRY: {
            edit_column('stakeholder_registry');
            break;
        }
        case ADD_STAKEHOLDER_REGISTRY: {
            add_column('stakeholder_registry')
            break;
        }
        case EDIT_ASSUMPTION_LOG: {
            edit_column('assumption_log');
            break;
        }
        case ADD_ASSUMPTION_LOG: {
            add_column('assumption_log')
            break;
        }
        case EDIT_STAKEHOLDER_ENGAGEMENT: {
            edit_column('stakeholder_engagement');
            break;
        }
        case ADD_STAKEHOLDER_ENGAGEMENT: {
            add_column('stakeholder_engagement')
            break;
        }
        case EDIT_RESOURCE_BREAKDOWN: {
            edit_column('resource_breakdown');
            break;
        }
        case ADD_RESOURCE_BREAKDOWN: {
            add_column('resource_breakdown')
            break;
        }
        case EDIT_QUALITY_OBJECTIVES: {
            edit_column('quality_objectives');
            break;
        }
        case ADD_QUALITY_OBJECTIVES: {
            add_column('quality_objectives')
            break;
        }
        case EDIT_DELIVERABLE_INFORMATION: {
            edit_column('deliverable_information');
            break;
        }
        case ADD_DELIVERABLE_INFORMATION: {
            add_column('deliverable_information')
            break;
        }
        case ADD_RESOURCE_MANAGEMENT: {
            add_column('resource_management', {init: {0: "1.1.1"}})
            break;
        }
        case EDIT_RESOURCE_MANAGEMENT: {
            edit_column('resource_management')
            break;
        }
        case ADD_RAM: {
            add_column('ram')
            break;
        }
        case EDIT_RAM: {
            edit_column('ram')
            break
        }
        case ADD_COMMUNICATION: {
            add_column('communication')
            break;
        }
        case EDIT_COMMUNICATION: {
            edit_column('communication')
            break
        }
        case ADD_COMMUNICATION_ITEM: {
            add_column('communication_items')
            break;
        }
        case EDIT_COMMUNICATION_ITEM: {
            edit_column('communication_items')
            break
        }
        case ADD_RISK: {
            add_column('risk')
            break;
        }
        case EDIT_RISK: {
            edit_column('risk')
            break
        }
        case EDIT_CTQ: {
            let {value, row, column} = action
            new_state.ctq[column][row] = value
            // alert("CTQ Changed")
            break;
        }
        case ADD_CTQ: {
            let size = new_state.ctq[1].length
            new_state.ctq[1].push(`phase ${size}`)
            new_state.ctq[2].push(`deliverable ${size}`)
            break;
        }       
        case EDIT_WBS: {
            let {value} = action
            // process the root specially`
            if (action.value.position==="1.0") {
                new_state.wbs.text=value.value;
            } else {
                let child = find_child_in_wbs(new_state.wbs, action.value.position.split('.') )
                child.text = value.value
            }
            break;
        }
        case ADD_WBS: {
            let {value} = action;
            let to_add = {text: "Add description", children: [] };
            if (value==="1.0") {
                new_state.wbs.children.push(to_add);
            } else {
                let child = find_child_in_wbs(new_state.wbs, value.split('.') )
                child.children.push(to_add)
            }
            break;
        }
        case ADD_WBS_DICTIONARY: {
            add_column('wbs_dictionary')
            break;
        }
        case EDIT_WBS_DICTIONARY: {
            edit_column('wbs_dictionary')
            break
        }
        case ADD_COST_ESTIMATE: {
            add_column('cost_estimate')
            break;
        }
        case EDIT_COST_ESTIMATE: {
            edit_column('cost_estimate')
            break
        }
        case ADD_BUDGET: {
            add_column('budget')
            break;
        }
        case EDIT_BUDGET: {
            edit_column('budget')
            break
        }

        default:
            console.error("Unknown action", action.type, action)
    }
    return new_state
}

export default reducer;



