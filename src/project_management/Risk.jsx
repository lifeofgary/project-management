
import { H3 } from '../components/Components'
import TableForProject from "./TableForProject"
import { 
    EDIT_RISK,
    ADD_RISK,
  } from "./reducer";

import { legend } from "../Classes"

const Risk = props=>{
    return (
        <>
            <H3>Risk Registry</H3>
            <p className="text-gray-400">
                Slide 120 from section 2.  Page 151 of <i>Project Management</i>
            </p>

            <TableForProject {...props} 
                on_edit_action={EDIT_RISK}
                on_add_action={ADD_RISK}            
            />


            <div className={legend.title}>Legend</div>
            <div className="grid grid-cols-3">
                <div className={legend.header}>Risk Score</div>
                <div className={legend.header}>Level of Urgency</div>
                <div className={legend.header}>Managment Plan</div>

                <div className={legend.odd_row}>20-25</div>
                <div className={legend.odd_row}><span className="text-red-500">High Red</span></div>
                <div className={legend.odd_row}>Assign Risk Owner<br/>Place on Urgent List<br/>Review Weekly</div>

                <div className={legend.even_row}>9-19</div>
                <div className={legend.even_row}><span className="text-yellow-500">Moderate Yellow</span></div>
                <div className={legend.odd_row}>Assign Risk Owner<br/>Place on Watch List<br/>Review Weekly</div>

                <div className={legend.odd_row}>1-8</div>
                <div className={legend.odd_row}><span className="text-green-500">Low Green</span></div>
                <div className={legend.odd_row}>Assign Risk Owner<br/>Place on Watch List<br/>Review Weekly</div>
            </div>

            <div className="text-gray-500">
                <p>Risk score = Probabilty * Impact Risk</p>
                <p>
                    Probabilty (1-5)<br/>
                    Impact Risk (1-5)
                </p>
            </div>

        </>
    )
}

export default Risk