
import { find_child_in_wbs } from "./helpers"

const Spacer = () => <div className={`col-span-1`}/>

// Does the array only contain nulls?
const null_array = a => a.filter( b=>b!==null ).length===0

const create_first_row = ({Cell, table, dispatch, ...props}) => {
    // Calculate the root node.  It needs to be centered
    let row_of_cells = []
    let center = Math.floor((table.children.length)/2);
    for (let index=0; index< table.children.length; index++) {
        row_of_cells.push( (index===center) ?
            <Cell
                key={"1.0"}
                title={"1.0"}
                text={table.text}
                dispatch={dispatch}
                depth={0}
                {...props}
           />
        :
            <Spacer key={`spacer-${index}`}/>
        )
    }
    return row_of_cells;
}

const calc_cells = ({Cell, data, dispatch, key: key_func, ...props}) => {
    let row = []
    for (let index=0; index<data.length; index++) {
        let key = key_func(index);

        data[index] ?
        row.push(
            <Cell
                key={key}
                title={key}
                text={data[index].text}
                depth={Math.ceil( key.length/2)}
                dispatch={dispatch}
                {...props}
            />
        ) : row.push(<Spacer key={key}/>)
    }
    return row;
}

const create_row = ({Cell, table, dispatch, ...props}) => {
    let row = []
    let cols = table.children.length

    for (let i=0; true; i++) {
        // Add second level items
        let array_2=[]
        for (let j=0; j<cols; j++) {
            array_2.push(`${j+1}.${i+1}`);
        }
        let items = array_2.map( index=>find_child_in_wbs(table, index.split(".") ) )
        if (null_array(items))
            break;
        row.push( calc_cells({Cell, data: items, dispatch, key: index=>array_2[index], ...props}));   

        // Add third level items
        let array_3 = []
        for (let j=0; true; j++) {
            let row = []
            for (let k=0; k<cols; k++)
                row.push(`${k+1}.${i+1}.${j+1}`);

            items = row.map( index=>find_child_in_wbs(table, index.split(".") ) )
            if (null_array(items)) {
                break;
            }
            array_3 = array_3.concat(row)
        }
        items = array_3.map( index=>find_child_in_wbs(table, index.split(".") ) )
        row.push( calc_cells({Cell, data: items, dispatch,  key: index=>array_3[index], ...props}));
    }
    return row
}

const WbsTree = ({Cell, table, dispatch, ...props}) => {
    let cells = []

    cells.push(create_first_row({Cell, table, dispatch, ...props}) )

    let cols = table.children.length
    // Add the first level of items
    let array_1 = []
    for (let i=0; i<cols; i++)
        array_1.push(i+1);
 
    let items = array_1.map( index=>find_child_in_wbs(table, [index] ) )
    cells.push( calc_cells({Cell, data: items, dispatch, key: index=>`${index+1}`, ...props}));

    cells.push(create_row({Cell, table, dispatch, ...props}))
    return cells
}

export default WbsTree