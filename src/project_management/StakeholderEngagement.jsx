
import { H3 } from "../components/Components"
import { legend } from "../Classes"

import TableForProject from "./TableForProject"
import {
    EDIT_STAKEHOLDER_ENGAGEMENT,
    ADD_STAKEHOLDER_ENGAGEMENT,
  } from "./reducer";
  
const StakeholderEngagement = props => {
    return (
        <>
            <H3>Stakeholder Engagement</H3>
            <p className="text-gray-400">
                Slide 20 from section 2.  Page 23 of <i>Project Management</i>
            </p>


            <TableForProject 
                {...props} 
                on_edit_action={EDIT_STAKEHOLDER_ENGAGEMENT}
                on_add_action={ADD_STAKEHOLDER_ENGAGEMENT}
                    />

            <div className={legend.title}>Legend</div>

            <div className={legend.grid}>
                <div className={legend.header}>Role</div>
                <div className={legend.header}>Definition</div>

                <div className={legend.odd_row}>U</div>
                <div className={legend.odd_row}><b>Unaware:</b>  Stakeholder is unaware of the project</div>

                <div className={legend.even_row}>N</div>
                <div className={legend.even_row}><b>Neutral:</b> Stakeholder has yet to determine level of support</div>

                <div className={legend.odd_row}>R</div>
                <div className={legend.odd_row}><b>Resistant:</b> Stakeholder does not support the project</div>

                <div className={legend.even_row}>S</div>
                <div className={legend.even_row}><b>Supportive:</b> Stakeholder agrees with the project</div>

                <div className={legend.odd_row}>L</div>
                <div className={legend.odd_row}><b>Leading:</b> Supportive and actively engaged in the project </div>
            </div>
        </>
    )
}

export default StakeholderEngagement