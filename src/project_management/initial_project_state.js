

const init = {
  name: "New Project",
  charter: {
    headers: ["Charter Item", "Comments"],
    body:  [
      [{text: "Project Value"}, ""],
      [{text: "Project Goal"}, ""],
      [{text: "Project Value\nProsposition and \nBenefits"}, ""],
      [{text: "Problem or \n Opportunity \n Statement"}, ""],
      [{text: "Project Schedule"}, ""],
      [{text: "Project Budget \n Rought Order of Magnitude \n (+75%/-25%)"}, ""],
      [{text: "Project Manager"}, ""],
      [{text: "Approval \n Authority Sponsor"}, ""],
    ],
  },
  stakeholder_registry: {
    headers: ["Name", "Origanization", "Role", "R", "A", "C", "I", "Comments"],
    body: [[]],
  },
  assumption_log: {
    headers: ["Name", "Current State", "Desired State", "Strategy"],
    body: [[]],
  },
  schedule: {
    from: [],
    to: [ [] ]
  },
  stakeholder_engagement: {
    headers: ["Name", "Current State", "Desired State", "Strategy" ],
    body: [[]],
  },
  resource_breakdown: {
    headers: [
      "Activity/Work Package Identifier", 
      "Human Resources (HR)", 
      "Hours", 
      "HR Cost", 
      "Materials and Supplies", 
      "Materials and Supplies Cost",
      "Equipment",
      "Equipment Cost",
      "Other",
      "Total Cost",
    ],
    body: [[]],
  },
  quality_objectives: {
    headers: ["Quality Objectives", "Who's Responsible", "R", "A", "C", "I", "Comments"],
    body: [[]],
  },
  deliverable_information: {
    headers: [
      "BS Code of Account Identifier",
      "Deliverable Summary",
      "Quality Tools Recommended",
      "Application Standards",
      "QA Activities (Y/N)",
      "QC Activities (Y/N)",
      "Applicable Metrics",
      "Major Procedures/Guidelines Applicable",
    ],
    body: [[]]
  },
  resource_management: {
    headers: [
      "Code of\nAccount\nIdentifier", 
      "Resource Requirements",
      "Acquisition\nGuidance",
      "Organization\nResponsible",
      "Staffing\nOnboard",
      "Staffing\nRelease",
      "Training\nRequirements",
      "Team Development\nRequirements",
      "RACI",
      "Resource\nControls",
      "Recognition\nPlanning",
      "Comments",     
    ],
    body: [[]]
  },
  ram: {
    headers: [ "Activity/Group", "PM", "Customer", "Dev Team", "Test Team", "Vendor"],
    body: [[]]
  },
  communication:{
    headers: ["Communication\nItem Form\n(What)", "Owner", "Audience", "Timing", "Format", "Purpose"],
    body: [[]]
  },
  communication_items: {
    headers: ["Communication\nItem Form\n(What)", "Owner\n(Who Sender)", "Audience\n(Who Reciver)", "Timing\n(When)", "Format\nMeduim\n(How)\n(Where)", "Purpose\n(Why)"],
    body: []
  },
  risk: {
    headers: ["#", "Cause", "Event", "Impact", "Risk Owner", "Category", "Probablity\nRisk Rating", "Impact Risk\nRating", "Risk Score", "Trigger", "Response"],
    body: [[]]
  },
  ctq: [
      ['project goal'],
      ['phase 1'],
      ['deliverable 1'],
    ],
  wbs: {
    text: "Root",
    children: [{
      text: "Define Process (1.0)",
      children:[
        {
          text: "Inputs (1.1)", children: [
            {text: "first input (1.1.1)", children: []},
            {text: "second input (1.1.2)", children: []},
            {text: "third input (1.1.3)", children: []},
        ]},
        {
          text: "Outputs (1.2)", children: [
            {text: "first output (1.2.1)", children: []},
            {text: "second output (1.2.2)", children: []},
        ]},
      ]
    },
    {
        text: "Measure Process (2.0)",
        children:[
          {text: "Measure Outputs (2.1)", children: [
            {text: "measure first input (2.1.1)", children: []},
            {text: "measure second input (2.1.2)", children: []},
          ]},
          {text: "Measure Outputs (2.2)", children: [
            {text: "measure first output (2.2.1)", children: []},
            {text: "measure second output (2.2.2)", children: []},
          ]},
        ]
    },
  ]},
  wbs_dictionary: {
    headers: ["WBS Identifier", "Team Lead", "Activity Attributes", "Reference"],
    body: [[]]
  },
  cost_estimate: {
    headers: [
      "Estimated\nDirect Costs",
      "Estimated\nIndirect Costs",
      "Total Project Costs",
      "Category\n(Direct/Indirect)",
      "Quarter Needed\nComments"
    ],
    body: []
  }, 
  budget: {
    headers: [
      "WBS Code",
      "Personal\n(Direct)",
      "Equipment\n(Direct)",
      "Material\n(Direct)",
      "Supplies\n(Direct)",
      "Total Direct Costs",
      "Indirect Costs",
      "Contingency Cost",
      "Clarifying Notes" 
    ],
    body: []
  }
};

export default init;