
import Button from "../components/Button";

import EditibleCell from "../components/EditibleCell"

import { table as table_css, add_class } from "../Classes";

const is_object = objValue => objValue && typeof objValue === 'object' && objValue.constructor === Object;

const Cell = ({styles, dispatch, on_edit_action, row_index, column_index, cell, id: key}) => {

    if (!is_object(cell)) {
        add_class(styles, "hover:bg-gray-300")
        return (
            <EditibleCell
                styles={styles}
                tabIndex={key}
                key={key}
                text={cell}
                onEdit={value=>{
                    dispatch({
                        type: on_edit_action,
                        value,
                        row: row_index,
                        column: column_index
                    })
                }}
            />
        )
    }
    else {
        // return <div>B</div>
        return (
            <div key={key} className={styles}>
                {cell.text}
            </div>
        )
    }
}

const TableForProject =  ({
    table,
    dispatch,
    on_edit_action,
    on_add_action,
    color="blue"
}) => {
    let size = table.headers.length
    const headers = table.headers.map( (item, index)=><div key={index} className={table_css.header(color)}>{item}</div>)

    let rows = table.body.map( (row_content, row_index)=>{
        
        return row_content.map ( (cell, column_index)=> {
            let key=column_index+size*row_index;
            let styles = row_index%2===0 ? table_css.even_row(color) : table_css.odd_row(color);

            return <Cell key={key}
                id={key}
                {...{styles, dispatch, on_edit_action, row_index, column_index, cell}}       
            
            />
        })
    } )

    return (
        <>
            <div className={table_css.grid(size) }>
                {headers}
                {rows}
            </div>
            { on_add_action ? <Button size="xs"  onClick={()=>dispatch({type: on_add_action})} > + </Button> : " " }
        </>
    )
}


export default TableForProject