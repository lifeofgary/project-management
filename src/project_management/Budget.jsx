
import TableForProject from "./TableForProject"
import { ADD_BUDGET, EDIT_BUDGET } from "./reducer"

const Schedule = ({dispatch, table})=>{
    return (
        <div>
            <h3>Plan Cost Management</h3>
            <p className="text-gray-400">
                Slide 78 from section 2.
            </p>

            <TableForProject
                table={table}
                dispatch={dispatch}
                on_edit_action={EDIT_BUDGET}
                on_add_action={ADD_BUDGET}
            />

        </div>
    )
}

export default Schedule
