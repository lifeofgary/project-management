
import { H1, H2 } from '../components/Components';
import NavigationIcon from './NavigationIcon';
import { Outlet, useLocation } from 'react-router-dom';
import ProjectList from '../ProjectList';

import Initiate from "./Initiating"
import Plan from "./Plan"

import { useReducer } from "react"
import reducer, { SET_STATE } from './reducer';

const INITIATE = "/project-management/initiating"
const PLAN = "/project-management/plan"

const ProjectManagement = props => {
    // const [saved_project_state, save_project_state] = useLocalStorage("_test_projects2", JSON.stringify(initial_project_state,0,2))
    const [project_state, dispatch] = useReducer(reducer, null);
    const { pathname } = useLocation();

    return (
        <>
            <H1 className="mb-1"> Project Management </H1>

            <ProjectList state={project_state} set_state={state => dispatch({ type: SET_STATE, value: state })} />

            {project_state === null ? null :
                <>
                    <H2> Name: <span className="italic">{project_state.name}</span> </H2>
                    <Outlet />
                    <div className="flex flex-row my-2">
                        <NavigationIcon to={INITIATE} image="basket" text="Initialize" active={pathname.startsWith(INITIATE)} />
                        <NavigationIcon to={PLAN} image="settings" text="Plan" active={pathname.startsWith(PLAN)} end />
                    </div>
                    <Outlet />

                    <Initiate project_state={project_state} dispatch={dispatch} {...props} />
                    <Plan project_state={project_state} dispatch={dispatch} {...props} />
                </>
            }

        </>
    )
}

export default ProjectManagement