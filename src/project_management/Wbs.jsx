
import { legend } from "../Classes"
import { H4 } from "../components/Components"
import EditibleCell from "../components/EditibleCell"
import { EDIT_WBS, ADD_WBS, ADD_WBS_DICTIONARY, EDIT_WBS_DICTIONARY } from "./reducer"
import WbsExample, {WbsDictionaryExample} from "./examples/WbsExample"
import ExampleButton from "../components/ExampleButton"
import { FaPlus } from 'react-icons/fa';
import Button from "../components/Button"
import TableForProject from "./TableForProject"
import WbsTree from "./WbsTree"

const WbsCell = ({text, dispatch, title, depth=0}) => {
    let background, button_color
    let add_button = true
    switch (depth) {
        case 0: background="bg-amber-400"; button_color="text-amber-500"; break;
        case 1: background="bg-amber-300"; button_color="text-amber-400"; break;
        case 2: background="bg-amber-200"; button_color="text-amber-300"; break;
        default: background="bg-amber-100"; add_button = false;
    }
    let onClick = ()=>{
        dispatch({
            type: ADD_WBS,
            value: title,
        })
    }
    let button = add_button ? 
        <Button size="xs" amber transparent onClick={onClick} >
            <FaPlus className={`text-xs ${button_color}`}/>
        </Button> : null

    return (
        <div  className={`h-200 w-full border rounded ${background} border-zinc-400 px-2`}>
            <div>
                <span>{title} {button} </span>
                <EditibleCell
                    styles={''}
                    text={text}
                    onEdit={value=>{
                        dispatch({
                            type: EDIT_WBS,
                            value: {
                                value,
                                position: title,
                            }
                        })
                    }}
                />
            </div>
        </div>    
    )
}

let Wbs = ({table, dictionary, dispatch}) => {
    return (
        <div>

            <H4>Work Breakdown Schedule (WBS) </H4>
            <p className="text-gray-400">
                Slide 44 from section 2.  Page 52 of <i>Project Management</i>
            </p>

            <div className={`grid grid-cols-${table.children.length} gap-x-4 gap-y-2`}>
                <WbsTree
                    Cell={WbsCell}
                    table={table}
                    dispatch={dispatch}
                />
            </div>

            <ExampleButton>
                <WbsExample />
            </ExampleButton>

            <H4>WBS Dictionary</H4>

            <TableForProject  
                table={dictionary}
                on_edit_action={EDIT_WBS_DICTIONARY} 
                on_add_action ={ADD_WBS_DICTIONARY} 
                dispatch = {dispatch}                         
            />

            <ExampleButton>
                <WbsDictionaryExample />
            </ExampleButton>

            <div className={legend.title}>Legend</div>
            <div className="grid grid-cols-4">

                <div className={legend.header}>Code of Account Identifier</div>
                <div className={legend.header}>Team Lead</div>
                {/* <div className={legend.header}>Activity Attributes</div> */}
                <div className={legend.header}>Activity Attributes</div>
                {/* <span>Activity Attributes and Potential Entries<br/>* Provide require attributes.  Not all Work packages may require details</span> */}
                <div className={legend.header}>References</div>

                <div className={legend.odd_row}>1</div>
                <div className={legend.odd_row}>2</div>
                <div className={legend.odd_row}>3</div>
                <div className={legend.odd_row}>4</div>

                <div className={legend.even_row}>A</div>
                <div className={legend.even_row}>B</div>
                <div className={legend.even_row}>C</div>
                <div className={legend.even_row}>D</div>


            </div>

            <p> Risk: Green, Yellow, Red </p>
            <p> Funding: Funding sources by Chart of Account Identifer </p>
            <p> Status: Complete, Started, Not Started</p>
            <p> Team Assignments<br/>{     }RACI</p>
            <p> Procurement activities </p>

        </div>


    )
}

export default Wbs