import {table} from "../../Classes"

const CostEstimateExample = () => {
    let color = "zinc"

    return (
        <>
        <div className={table.grid(6)}>
            <div className={table.header(color)}> Cost Factor </div>
            <div className={table.header(color)}> Estimated<br/>Direct Costs </div>
            <div className={table.header(color)}> Esteimated<br/>Indirect Costs </div>
            <div className={table.header(color)}> Total Project Costs </div>
            <div className={table.header(color)}> Category<br/>(Direct/Indirect) </div>
            <div className={table.header(color)}> Quarter Needed<br/>Comments </div>

            <div className={table.odd_row(color)}>Personal (Vendor)</div>
            <div className={table.odd_row(color)}>$30,000</div>
            <div className={table.odd_row(color)}>$0</div>
            <div className={table.odd_row(color)}>$30,000</div>
            <div className={table.odd_row(color)}>Direct (200 hours x $150)</div>
            <div className={table.odd_row(color)}>2016 (Q1-Q4)</div>

            <div className={table.even_row(color)}>Personal (Internal)</div>
            <div className={table.even_row(color)}>$0</div>
            <div className={table.even_row(color)}>$108,000</div>
            <div className={table.even_row(color)}>$108,000</div>
            <div className={table.even_row(color)}>Indirect (900 hours X $120)</div>
            <div className={table.even_row(color)}></div>

            <div className={table.odd_row(color)}>Equipment</div>
            <div className={table.odd_row(color)}>$25,000</div>
            <div className={table.odd_row(color)}>$0</div>
            <div className={table.odd_row(color)}>$25,000</div>
            <div className={table.odd_row(color)}>Direct</div>
            <div className={table.odd_row(color)}>2016 Q3</div>

            <div className={table.even_row(color)}>Materials</div>
            <div className={table.even_row(color)}>$12,000</div>
            <div className={table.even_row(color)}>$0</div>
            <div className={table.even_row(color)}>$12,000</div>
            <div className={table.even_row(color)}>Direct</div>
            <div className={table.even_row(color)}>$1000 per quarter</div>

            <div className={table.odd_row(color)}>Supplies</div>
            <div className={table.odd_row(color)}>$600</div>
            <div className={table.odd_row(color)}>$0</div>
            <div className={table.odd_row(color)}>$600</div>
            <div className={table.odd_row(color)}>Direct</div>
            <div className={table.odd_row(color)}>$50 per quarter</div>

            <div className={table.even_row(color)}>Devlopment/Construction</div>
            <div className={table.even_row(color)}>$35,000</div>
            <div className={table.even_row(color)}>$0</div>
            <div className={table.even_row(color)}>$35,000</div>
            <div className={table.even_row(color)}>Direct</div>
            <div className={table.even_row(color)}>Room renovation 2018 Q3</div>

            <div className={table.odd_row(color)}>Travel</div>
            <div className={table.odd_row(color)}>$6,000</div>
            <div className={table.odd_row(color)}>$0</div>
            <div className={table.odd_row(color)}>$6,000</div>
            <div className={table.odd_row(color)}>Direct</div>
            <div className={table.odd_row(color)}>First 2 quarters 2016</div>

            <div className={table.even_row(color)}>Training</div>
            <div className={table.even_row(color)}>$0</div>
            <div className={table.even_row(color)}>$84,000</div>
            <div className={table.even_row(color)}>$84,000</div>
            <div className={table.even_row(color)}>Indirect (70 hours x $120)</div>
            <div className={table.even_row(color)}>2016 Q4 Not part of budget</div>

            <div className={table.odd_row(color)}>Support</div>
            <div className={table.odd_row(color)}>$10,000</div>
            <div className={table.odd_row(color)}>$0</div>
            <div className={table.odd_row(color)}>$10,000</div>
            <div className={table.odd_row(color)}>Direct</div>
            <div className={table.odd_row(color)}>2018 Q4</div>

            <div className={table.even_row(color)}>License/Permites</div>
            <div className={table.even_row(color)}>$0</div>
            <div className={table.even_row(color)}>$0</div>
            <div className={table.even_row(color)}>$0</div>
            <div className={table.even_row(color)}>N/A</div>
            <div className={table.even_row(color)}></div>

            <div className={table.header(color)}> Total </div>
            <div className={table.header(color)}> $118,600 </div>
            <div className={table.header(color)}> $192,000 </div>
            <div className={table.header(color)}> $310,600 </div>
            <div className={table.header(color)}>  </div>
            <div className={table.header(color)}>  </div>

        </div>
        <br/>
        <div className={table.grid(3)}>
            <div className={table.header(color)}> Contigency Reserves </div>
            <div className={table.even_row(color)}>10% Cost Basis</div>
            <div className={table.header(color)}> $11,960 </div>

            <div className={table.header(color)}> Management Reserves </div>
            <div className={table.even_row(color)}>5% Cost Basis</div>
            <div className={table.header(color)}> $5,930 </div>

       </div>

    </>
    )
}

export default CostEstimateExample