import {table} from "../../Classes"

const StakeholderRegistryExample = ()=>{
    let color = "zinc"
    return (
        <div className={table.grid(8)}>
            <div className={table.header(color)}> Name </div>
            <div className={table.header(color)}> Origanization </div>
            <div className={table.header(color)}> Role </div>
            <div className={table.header(color)}> R </div>
            <div className={table.header(color)}> A </div>
            <div className={table.header(color)}> C </div>
            <div className={table.header(color)}> I </div>
            <div className={table.header(color)}> Comments </div>

            <div className={table.odd_row(color)}>Juan Valdez</div>
            <div className={table.odd_row(color)}>PME</div>
            <div className={table.odd_row(color)}>Project Manager</div>
            <div className={table.odd_row(color)}>X</div>
            <div className={table.odd_row(color)}>X</div>
            <div className={table.odd_row(color)}></div>
            <div className={table.odd_row(color)}></div>
            <div className={table.odd_row(color)}></div>

            <div className={table.even_row(color)}>Jimmy Java</div>
            <div className={table.even_row(color)}>CEO</div>
            <div className={table.even_row(color)}>Sponsor</div>
            <div className={table.even_row(color)}></div>
            <div className={table.even_row(color)}>X</div>
            <div className={table.even_row(color)}></div>
            <div className={table.even_row(color)}>X</div>
            <div className={table.even_row(color)}>Accountable Approval/Funding</div>

            <div className={table.odd_row(color)}>Barbara Bean</div>
            <div className={table.odd_row(color)}>Ops</div>
            <div className={table.odd_row(color)}>Core Team Member</div>
            <div className={table.odd_row(color)}>X</div>
            <div className={table.odd_row(color)}></div>
            <div className={table.odd_row(color)}>X</div>
            <div className={table.odd_row(color)}></div>
            <div className={table.odd_row(color)}>Focus Group Lead</div>

            <div className={table.even_row(color)}>Rajau Roaster</div>
            <div className={table.even_row(color)}>Ops</div>
            <div className={table.even_row(color)}>Core Team Member</div>
            <div className={table.even_row(color)}>X</div>
            <div className={table.even_row(color)}></div>
            <div className={table.even_row(color)}>X</div>
            <div className={table.even_row(color)}></div>
            <div className={table.even_row(color)}>Lead Vendor Liason/Manager</div>

            <div className={table.odd_row(color)}>Mary Mocha</div>
            <div className={table.odd_row(color)}>Ops</div>
            <div className={table.odd_row(color)}>Core Team Member</div>
            <div className={table.odd_row(color)}>X</div>
            <div className={table.odd_row(color)}></div>
            <div className={table.odd_row(color)}>X</div>
            <div className={table.odd_row(color)}></div>
            <div className={table.odd_row(color)}>Planning Lead</div>

            <div className={table.even_row(color)}>Larry Latte</div>
            <div className={table.even_row(color)}>Marketing</div>
            <div className={table.even_row(color)}>Manage Marketing Compaign</div>
            <div className={table.even_row(color)}>X</div>
            <div className={table.even_row(color)}>X</div>
            <div className={table.even_row(color)}></div>
            <div className={table.even_row(color)}>X</div>
            <div className={table.even_row(color)}>Marketing Lead</div>

            <div className={table.odd_row(color)}>Wally Wifi</div>
            <div className={table.odd_row(color)}>TCTM IT</div>
            <div className={table.odd_row(color)}>Develop App</div>
            <div className={table.odd_row(color)}>X</div>
            <div className={table.odd_row(color)}>X</div>
            <div className={table.odd_row(color)}></div>
            <div className={table.odd_row(color)}>X</div>
            <div className={table.odd_row(color)}>External Vendor</div>

        </div>
    )
}

export default StakeholderRegistryExample;