import {table} from "../../Classes"

function ProjectCharterExample() {
    let color = "zinc"
    return (
        <div className={table.grid(2)}>
            <div className={table.header(color)}> Charter Item </div>
            <div className={table.header(color)}> Comments </div>

            <div className={table.odd_row(color)}>Project Name</div>
            <div className={table.odd_row(color)}>Develop Coffee Shop Application (App)</div>

            <div className={table.even_row(color)}>Project Goal</div>
            <div className={table.even_row(color)}>
                <p>Need: Our Coffee Shop requires a business application to be made available to our customers</p>
                <p>Our Goal/Scope:  Develop a functional app within the next three months</p>
            </div>

            <div className={table.odd_row(color)}>
                    Project Value<br/>
                    Prosposition and <br/>
                    Benefits
            </div>
            <div className={table.odd_row(color)}>
                <ul className="list-disc">
                    <li>Improved customer satisfaction</li>
                    <li>Improved sales and revenue</li>
                    <li>Improved visibility and credibility</li>
                    <li>Potential for improved marketing reach</li>
                    <li>Improved time to service customers</li>
                </ul>
                <p>Our Goal/Scope:  Develop a functional app within the next three months</p>
            </div>

            <div className={table.even_row(color)}>
                Project Value Prosposition and Benefits
            </div>
            <div className={table.even_row(color)}>
                Benefits
                <ul className="list-disc">
                    <li>Improved customer satisfaction</li>
                    <li>Improved sales and revenue</li>
                    <li>Improved visibility and credibility</li>
                    <li>Potential for improved marketing reach</li>
                    <li>Improved time to service customers</li>
                </ul>                        
            </div>

            <div className={table.odd_row(color)}>
                Problem or<br/>
                Opportunity <br/>
                Statement
            </div>
            <div className={table.odd_row(color)}>
                Problem: Other competitors have a functional application that is increasing their sales.  We are lagging behind
            </div>

            <div className={table.even_row(color)}>
                Project Schedule
            </div>
            <div className={table.even_row(color)}>
                <ul className="list-disc">
                    <li>Project Start:  1 Feb 2020</li>
                    <li>Design Complete: 15 Feb 2020</li>
                    <li>Development Complete: 15 Mar 2020</li>
                    <li>Test Complete: 31 Mar 2020</li>
                    <li>Launch Complete:  30 Apr 2020</li>
                </ul>                        
            </div>

            <div className={table.odd_row(color)}>
                Project Manager
            </div>
            <div className={table.odd_row(color)}>
                Juan Valdez          
            </div>

            <div className={table.even_row(color)}>
                Approval /<br/>
                Authority Sponsor
            </div>
            <div className={table.even_row(color)}>
                Jimmy Java
            </div>



        </div>
    ) 
}

export default ProjectCharterExample;
