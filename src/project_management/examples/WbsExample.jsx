import { table } from "../../Classes"

const Spacer = ({size=1}) => {
    return (
        <div className={`col-span-${size}`}></div>
    )
}

const Cell = ({title, text, background="bg-gray-100"}) => {
    return (
        <div  className={`h-200 w-full border rounded ${background} border-zinc-400 px-2`}>
            <div>
                <span>{title}</span>
                <div>{text}</div>
            </div>
        </div>    
    )
}

const WbsExample = ()=>{
    return (
        <div className={`grid grid-cols-5 gap-x-4 gap-y-2`}>
            <Spacer size="2"/>
            <Cell title="1.0" text="Develop ABC Process" background="bg-gray-300"/>
            <Spacer size="2"/>

            <Cell title="1.1" text="Define Process" background="bg-gray-200"/>
            <Cell title="2.1" text="Measure Process" background="bg-gray-200"/>
            <Cell title="3.1" text="Analyze  Process" background="bg-gray-200"/>
            <Cell title="4.1" text="Improve Process" background="bg-gray-200"/>
            <Cell title="5.1" text="Control Process" background="bg-gray-200"/>

            <Cell title="1.1.1" text="Define Process Inputs & Outputs"/>
            <Cell title="2.1.1" text="Measure Process Outputs"/>
            <Cell title="3.1.1" text="Develop Program Statement"/>
            <Cell title="4.1.1" text="Develop Progress Charter"/>
            <Cell title="5.1.1" text="Finalize Process Maps"/>

            <Cell title="1.1.2" text="Develop SIPOC"/>
            <Cell title="2.1.2" text="Document Shortfalls"/>
            <Cell title="3.1.2" text="Peform Root Cause Analysis"/>
            <Cell title="4.1.2" text="Develop Project Plans"/>
            <Cell title="5.1.2" text="Communicate Changes"/>

            <Cell title="1.1.3" text="Develop Initial Process Maps"/>
            <Cell title="2.1.3" text="Prioritize Improvement Ideas"/>
            <Cell title="3.1.3" text="Identify Root Causes"/>
            <Cell title="4.1.3" text="Execute Project Plans"/>
            <Cell title="5.1.3" text="Conduct Training"/>

            <Cell title="1.1.4" text="Walk the Process"/>
            <Spacer/>
            <Cell title="3.1.4" text="Analyze and Validate Data"/>
            <Cell title="4.1.4" text="Perform Quality Control"/>
            <Cell title="5.1.4" text="Share Lessons Learned"/>

            <Cell title="1.1.5" text="Update Process Maps & Narrative"/>
            <Spacer/>
            <Cell title="3.1.5" text="Define Potential Soluctions"/>
            <Cell title="4.1.5" text="Elimintate Variance"/>
            <Cell title="5.1.5" text="Define Next Steps"/>

        </div>


    )
}

export const WbsDictionaryExample = ()=> {
    let color = "zinc"
    return (
        <div className={table.grid(4)}>
            <div className={table.header(color)}>WBS Identifier</div>
            <div className={table.header(color)}>Team Lead</div>
            <div className={table.header(color)}>Activity Attributes</div>
            <div className={table.header(color)}>Reference</div>

            <div className={table.odd_row(color)}>1.1.1</div>
            <div className={table.odd_row(color)}>Jose Lupez</div>
            <div className={table.odd_row(color)}>
                1. Meet with Customers - ensure key process outputs are defined specifically<br/>
                2. Inventory current inputs require to support outpusts.  Define soureds</div>
            <div className={table.odd_row(color)}>FY2016 Customer Satisfaction report</div>

            <div className={table.even_row(color)}>1.1.2</div>
            <div className={table.even_row(color)}>Tammy Bowzer</div>
            <div className={table.even_row(color)}>
                1. Map out initial process requirements<br/>
                2. Trace from Supplier - Inputs - Process Activities - Output - Customers<br/>
                3. Process maps MUST address all SIPOC criteria
            </div>
            <div className={table.even_row(color)}>Six Sigma memory Jogger</div>

            <div className={table.odd_row(color)}>1.1.3</div>
            <div className={table.odd_row(color)}>Jeff Jennings</div>
            <div className={table.odd_row(color)}>
                1. Map in Visio<br/>
                2. Use basic flowchart types<br/>
                3. Map using swimlanes
            </div>
            <div className={table.odd_row(color)}>See swim lane examples for vendor management process on SharePoint</div>

            <div className={table.even_row(color)}>1.1.4</div>
            <div className={table.even_row(color)}>Jose Lupez</div>
            <div className={table.even_row(color)}>
                1. Schedule one hour meet with process team and customer representatives<br/>
                2. Walk through the process<br/>
                3. Document gaps omissions.  non-value and activities and questions
            </div>
            <div className={table.even_row(color)}>Stakeholder and RECI designators</div>

            <div className={table.odd_row(color)}>1.1.5</div>
            <div className={table.odd_row(color)}>Jeff Jennings</div>
            <div className={table.odd_row(color)}>
                1. Schedule one hour meet with process team and customer representativUpdate initial process maps<br/>
                2. Ensure all issues discovered in 1.1.4 are addressed <br/>
                3. Assemble process measurement teams
            </div>
            <div className={table.odd_row(color)}>Stakeholder and RECI designators</div>

        </div>
    )
}

export default  WbsExample