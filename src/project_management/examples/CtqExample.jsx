
const cell_style = weight => `p-4 text-gray-500 bg-amber-${weight} border mx-2 my-1 rounded border-zinc-700`

const CtqExample = () => {
    return (
        <div className={`grid grid-rows-5 grid-flow-col min-w-fill`}>
            {/* Spacing placeholder */}
            <div className={`row-span-2 `}/>    
            <div className={cell_style(100) }>
                Project Goal
                <div>
                    Create a new product
                </div>
            </div>
            {/* Spacing placeholder */}
            <div className={`row-span-2 `}></div>

            <div className={cell_style(200)}>
                Key Phase 1
                <div>
                    Define and Design
                </div>
            </div>
            <div className={cell_style(200)}>
                Key Phase 2
                <div>
                    Develop
                </div>
            </div>
            <div className={cell_style(200)}>
                Key Phase 3
                <div>
                    Test
                </div>
            </div>
            <div className={cell_style(200)}>
                Key Phase 4
                <div>
                    Launch
                </div>
            </div>
            <div className={cell_style(200)}>
                Key Phase 5
                <div>

                </div>
            </div>

            <div className={cell_style(300)}>
                Measureable Deliverable 1
                <div>
                    <ol>
                        <li>1. Identify all features </li>
                        <li>2. Gain feature appoval</li>
                        <li>3. Develop initial design</li>
                        <li>4. Solict feedback and sign off</li>
                    </ol>
                </div>
            </div>
            <div className={cell_style(300)}>
                Measureable Deliverable 2
                <div>
                    <ol>
                        <li>1. Develop initial prototype </li>
                        <li>2. Present prototype - solicate feedback</li>
                        <li>3. Make adjustment as required</li>
                        <li>4. Ready to test</li>
                    </ol>
                </div>
            </div>
            <div className={cell_style(300)}>
                Measureable Deliverable 3
                <div>
                    <ol>
                        <li>1. Test initail prototype </li>
                        <li>2. Document issues </li>
                        <li>3. Address issues</li>
                        <li>4. Prepair for manufucture</li>
                    </ol>
                </div>
            </div>
            <div className={cell_style(300)}>
                Measureable Deliverable 4
                <div>
                    <ol>
                        <li>1. Manufacture product </li>
                        <li>2. Destribute product </li>
                        <li>3. Solicate feedback/adjust if needed</li>
                        <li>4. Close out and preform lessons learned</li>
                    </ol>
                </div>
            </div>
            <div className={cell_style(300)}>
                Measureable Deliverable 5
            </div>
        </div>
    )
}

export default CtqExample