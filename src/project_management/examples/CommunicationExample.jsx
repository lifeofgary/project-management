
import {table} from "../../Classes"
import { H3 } from "../../components/Components"

const CommunicationExample = () => {
    let color = "zinc"

    return (
        <>
            <H3>Communication Management</H3>
            <div className={table.grid(6)}>

                <div className={table.header(color)}> Communication<br/>Item Form<br/>(What) </div>
                <div className={table.header(color)}> Owner<br/>(Who Sender) </div>
                <div className={table.header(color)}> Audience<br/>(Who Reciver) </div>
                <div className={table.header(color)}> Timing </div>
                <div className={table.header(color)}> Format </div>
                <div className={table.header(color)}> Purpose </div>

                <div className={table.odd_row(color)}>Weekly Project Status Meet</div>
                <div className={table.odd_row(color)}>PM </div>
                <div className={table.odd_row(color)}>Core Team<br/>Members </div>
                <div className={table.odd_row(color)}>Weekly<br/>Wednesday at 2:00pm<br/>Continuious </div>
                <div className={table.odd_row(color)}>Meeting in Building 5<br/>Room 11 </div>
                <div className={table.odd_row(color)}>
                    Review status, discuss open<br/>
                    issue/risk, review project<br/>
                    deliverables
                </div>

                <div className={table.even_row(color)}>Sponsor 1:1 Meeting </div>
                <div className={table.even_row(color)}>PM </div>
                <div className={table.even_row(color)}>Sponsor and key<br/>Senior<br/>Management </div>
                <div className={table.even_row(color)}>biweekly<br/>Thursday at 9:00am<br/>Continuious </div>
                <div className={table.even_row(color)}>Meeting in Building 4<br/>Room 4 </div>
                <div className={table.even_row(color)}>
                    Update the project sponsor on<br/>
                    project status, discuss help<br/>
                    wanted, review change request<br/>
                    etc
                </div>

                <div className={table.odd_row(color)}>Weekly Project Status Report </div>
                <div className={table.odd_row(color)}>PM and Team </div>
                <div className={table.odd_row(color)}>Core team<br/>Members </div>
                <div className={table.odd_row(color)}>Weekly<br/>Due by Friday at 10:00 am<br/>Continuious </div>
                <div className={table.odd_row(color)}>Email and SharePoint </div>
                <div className={table.odd_row(color)}>
                    Provide overview of key project<br/>
                    status issues on PM
                </div>

                <div className={table.even_row(color)}>Change ControlBoard (CCB) </div>
                <div className={table.even_row(color)}>Sponsor </div>
                <div className={table.even_row(color)}>PM, team and<br/>CCB Members </div>
                <div className={table.even_row(color)}>Biweekly<br/>Monday at 10:30 am<br/>Continuious </div>
                <div className={table.even_row(color)}>Meeting in Building 4<br/>Rom 235 </div>
                <div className={table.even_row(color)}>
                    Review change requests<br/>
                    Approve or disapprove
                </div>

                <div className={table.odd_row(color)}>Vendor Source Selection</div>
                <div className={table.odd_row(color)}>PM</div>
                <div className={table.odd_row(color)}>PM Contacting Sponsor<br/>Core Team</div>
                <div className={table.odd_row(color)}>Mar 21 at 10:00am<br/>One-Time<br/>Continuious</div>
                <div className={table.odd_row(color)}>Meeting in Building 4<br/>Rom 235</div>
                <div className={table.odd_row(color)}>
                    Select consulting support group<br/>
                    to support design
                </div>
            </div>
        </>
    )
}

export default CommunicationExample