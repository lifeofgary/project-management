import ProjectCharter from "./ProjectCharter";
import StakeholderRegistry from "./StakeholderRegistry"
import AssumptionLog from "./AssumptionLog"
import { H2 } from "../components/Components";
import { Routes, Route, Outlet, useLocation } from "react-router-dom";
import NavigationIcon from "./NavigationIcon";

const INITIATE = "initiating"
const CHARTER = "charter"
const STAKEHOLDER_REGISTRY = "stakeholder-registry"
const ASSUMPTIONS = "assumptions"

const InitNav = () => {

  const {pathname} = useLocation()

  return (
      <>
          <div className="flex flex-row">
              <NavigationIcon image="clipboard" text="Charter" to={CHARTER} active={pathname.endsWith(CHARTER)}  />
              <NavigationIcon image="book" text="Stakeholder Registry" to={STAKEHOLDER_REGISTRY} active={pathname.endsWith(STAKEHOLDER_REGISTRY)} />
              <NavigationIcon image="rocket" text="Assumption Log" to={ASSUMPTIONS} no_example active={pathname.endsWith(ASSUMPTIONS)} end/>
          </div>        
          <H2> Initiating </H2>
          <Outlet/>
      </>
  )
}

const Initiate = ({project_state, dispatch}) => {
    return (<>
      <Routes>
        <Route path={`${INITIATE}/*`} element={<InitNav />} >
            <Route path={CHARTER} element={<ProjectCharter  table={project_state.charter}  dispatch={dispatch}/> } />
            <Route path={STAKEHOLDER_REGISTRY} element={<StakeholderRegistry  table={project_state.stakeholder_registry}  dispatch={dispatch}/> } />
            <Route path={ASSUMPTIONS} element={<AssumptionLog  table={project_state.assumption_log}  dispatch={dispatch} end/> } />
        </Route>  
      </Routes>
    </>)
}
    
export default Initiate