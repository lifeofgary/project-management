
import { ADD_COST_ESTIMATE, EDIT_COST_ESTIMATE } from "./reducer"
import TableForProject from "./TableForProject"
import ExampleButton from "../components/ExampleButton"
import CostEstimateExample from "./examples/CostEstimateExample"

const Schedule = ({dispatch, table})=>{
    return (
        <div>
            <h3>Plan Cost Estimate</h3>
            <p className="text-gray-400">
            Slide 76 from section 2.  Page 81 of <i>Project Management</i>
            </p>
            <TableForProject
                table={table}
                dispatch={dispatch}
                on_edit_action={EDIT_COST_ESTIMATE}
                on_add_action={ADD_COST_ESTIMATE}
            />

            <ExampleButton>
                <CostEstimateExample />
            </ExampleButton>
        </div>
    )
}

export default Schedule
