
import RaciLegend from './RaciLegend'
import { H3 } from '../components/Components'
import TableForProject from "./TableForProject"
import { 
    EDIT_RESOURCE_MANAGEMENT,
    ADD_RESOURCE_MANAGEMENT,
  } from "./reducer";
  
const ResourceManagement = props=>{
    return (
        <>
            <H3>Resource Management</H3>

            <TableForProject {...props} 
                on_edit_action={EDIT_RESOURCE_MANAGEMENT}
                on_add_action={ADD_RESOURCE_MANAGEMENT}            
            />

            <RaciLegend/>
        </>
    )
}

export default ResourceManagement