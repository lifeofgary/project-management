import StakeholderEngagement from "./StakeholderEngagement"
import Ctq from "./Ctq"
import Wbs from "./Wbs"
import Schedule from "./Schedule"
// import CostManagement from "./CostManagement"
import CostEstimate from "./CostEstimate"
import Budget from "./Budget"
import ResourceBreakdown from "./ResourceBreakdown"
import QualityManagement from "./QualityManagement"
import ResourceManagement from "./ResourceManagement"
import Ram from "./Ram"
import Communication from "./Communication"
import Risk from "./Risk"
import { H2 } from "../components/Components";

import { Routes, Route, Outlet, useLocation } from "react-router-dom";
import NavigationIcon from "./NavigationIcon";

const PLAN = "plan"

const ENGAGEMENT = "engagement-plan"
const CTQ = "critical-to-quality"
const WBS = "work-breakdown-schedule"
const SCHEDULE = "schedule"
const COST = "cost"
const BUDGET = "budget"
const RESOURCE_BREAKDOWN = "resource-breakdown"
const QUALITY = "quality"
const RESOURCE_MANAGEMENT = "resource-management"
const RAM = "resource-acquisition-management"
const COMMUNICATION = "communication"
const RISK = "risk"

const PlanNav = () => {
    const { pathname } = useLocation()

    return (
        <>
            <div className="flex flex-row">
                <NavigationIcon image="truck" text="Engage" to={ENGAGEMENT} active={pathname.endsWith(ENGAGEMENT)} />
                <NavigationIcon image="magic" text="CTQ" to={CTQ} active={pathname.endsWith(CTQ)} />
                <NavigationIcon image="map" text="WBS" to={WBS} active={pathname.endsWith(WBS)} />
                <NavigationIcon image="clocks" text="Schedule" to={SCHEDULE} in_progress active={pathname.endsWith(SCHEDULE)} />
                <NavigationIcon image="money" text="Cost" to={COST} active={pathname.endsWith(COST)} />
                <NavigationIcon image="card" text="Budget" to={BUDGET} no_example active={pathname.endsWith(BUDGET)} />
                <NavigationIcon image="calculator" text="Resource Breakdown" to={RESOURCE_BREAKDOWN} no_example active={pathname.endsWith(RESOURCE_BREAKDOWN)} />
                <NavigationIcon image="safe" text="Quality" to={QUALITY} no_example active={pathname.endsWith(QUALITY)} />
                <NavigationIcon image="trip-bag" text="Resource Management" to={RESOURCE_MANAGEMENT} no_example active={pathname.endsWith(RESOURCE_MANAGEMENT)} />
                <NavigationIcon image="loop" text="RAM" to={RAM} no_example active={pathname.endsWith(RAM)} />
                <NavigationIcon image="mail" text="Communication" to={COMMUNICATION} active={pathname.endsWith(COMMUNICATION)} />
                <NavigationIcon image="dynamite" text="Risk" to={RISK} no_example end active={pathname.endsWith(RISK)} />
            </div>
            <H2> Planning </H2>
            <Outlet />

        </>
    )
}

const Plan = ({ project_state, dispatch, ...props }) => {

    return (<>
        <Routes>
            <Route path={`${PLAN}/*`} element={<PlanNav />} >
                <Route path={ENGAGEMENT} element={<StakeholderEngagement table={project_state.charter} dispatch={dispatch} />} />
                <Route path={CTQ} element={<Ctq data={project_state.ctq} dispatch={dispatch} />} />
                <Route path={WBS} element={<Wbs table={project_state.wbs} dispatch={dispatch} dictionary={project_state.wbs_dictionary} end />} />
                <Route path={SCHEDULE} element={<Schedule table={project_state.wbs} schedule={project_state.schedule} dispatch={dispatch} {...props} />} />
                <Route path={COST} element={<CostEstimate table={project_state.cost_estimate} dispatch={dispatch} />} />
                <Route path={BUDGET} element={<Budget table={project_state.budget} dispatch={dispatch} />} />
                <Route path={RESOURCE_BREAKDOWN} element={<ResourceBreakdown table={project_state.resource_breakdown} dispatch={dispatch} />} />
                <Route path={QUALITY} element={<QualityManagement table={project_state.quality_objectives} dispatch={dispatch} deliverables={project_state.deliverable_information} />} />
                <Route path={RESOURCE_MANAGEMENT} element={<ResourceManagement table={project_state.resource_management} dispatch={dispatch} />} />
                <Route path={RAM} element={<Ram table={project_state.ram} dispatch={dispatch} />} />
                <Route path={COMMUNICATION} element={<Communication table={project_state.communication} dispatch={dispatch} items={project_state.communication_items} />} />
                <Route path={RISK} element={<Risk table={project_state.risk} dispatch={dispatch} />} />
            </Route>
        </Routes>

    </>)
}

export default Plan