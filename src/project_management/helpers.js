
export const find_child_in_wbs = (container, index_array) => {
    let index = index_array.shift()-1;
    if (!container.children || !container.children[index])
        return null;

    if (index_array.length===0)
        return container.children[index];
    return find_child_in_wbs(container.children[index], index_array);
}