import { H3, H4 } from "../components/Components"
import TableForProject from "./TableForProject"
import RaciLegend from "./RaciLegend"

import {
    EDIT_QUALITY_OBJECTIVES, 
    ADD_QUALITY_OBJECTIVES,
    EDIT_DELIVERABLE_INFORMATION,
    ADD_DELIVERABLE_INFORMATION,
  } from "./reducer";
  

const QualityManagement = props=> {
    return (
        <>
            <H3>Quality Management Plans</H3>
            <p className="text-gray-400">
                Slide 88 from section 2
            </p>

            <TableForProject 
                table={props.table}
                on_edit_action={EDIT_QUALITY_OBJECTIVES}
                on_add_action={ADD_QUALITY_OBJECTIVES}
                dispatch = {props.dispatch}                      
            />

            <H4>Deliverables Information</H4>
            <TableForProject
                table={props.deliverables}
                on_edit_action={EDIT_DELIVERABLE_INFORMATION} 
                on_add_action ={ADD_DELIVERABLE_INFORMATION} 
                dispatch = {props.dispatch}              
            />

            <RaciLegend/>
        </>
    )
}

export default QualityManagement