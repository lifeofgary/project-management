
// import WbsTree from "./WbsTree"
import { useEffect } from "react"
import { Droppable, Draggable } from 'react-beautiful-dnd';
import { SCHEDULE } from "./reducer";

const Cell = ({ text, title, depth = 0, color = "orange", index }) => {
    let background

    switch (depth) {
        case 3: background = `bg-${color}-200`; break;
        case 1: background = `bg-${color}-400`; break;
        case 2: background = `bg-${color}-300`; break;
        default: background = `bg-${color}-100`; 
    }

    return (
        <Draggable key={title} draggableId={title} index={index}>
            {(provided, snapshot) => {
                return (

                    <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}

                        className={`h-200 w-full border rounded ${background} border-zinc-600 px-2 my-1`}
                    >
                        <div>
                            <span>
                                {title}
                            </span>
                            <div>{text}</div>
                        </div>
                    </div>
                )
            }}
        </Draggable>
    )
}

const MyList = ({ data }) => {
    return (
        <>
            {data.map((item, index) => {
                return < Cell key={item.index}
                    index={index}
                    title={item.index}
                    text={item.text}
                    depth={Math.ceil(item.index.length / 2)}
                />
            }
            )}
        </>

    )
}

const Target = ({id, state, color, children}) => {

    const colors = [
        "teal",
        "green",
        "cyan",
        "purple",
        "amber",
        "orange",
        "red", 
        "slate",
        "gray",
        "zinc",
    ]
    color ||= colors[ id % colors.length ]

    state = state[id]

    return (
        <Droppable droppableId={id}  >
            {(provided, snapshot) => {
                return (
                    <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        className={`flex-1 border rounded border-${color}-600 bg-${color}-100 px-2 mb-2 round boarder`}
                    >
                        {
                            state.length===0 ? <div className={`text-${color}-600`}>
                                    {children}
                                </div>
                            :
                                state.map( (item, index) => (
                                    < Cell key={item.index}
                                        index={index}
                                        color={color}
                                        title={item.index}
                                        text={item.text}
                                        depth={Math.ceil(item.index.length / 2)}
                                    />
                                ))
                        }   
                        {provided.placeholder}
                    </div>
                )
            }}
    </Droppable>        
    )
}

const Schedule = ({ table, dispatch, schedule,  drag_and_drop_state, drag_and_drop_dispatch }) => {
    useEffect(() => {
        drag_and_drop_dispatch({ type: "new", value: {table, schedule} })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect( ()=>{
        dispatch({ type: SCHEDULE, value: drag_and_drop_state.schedule })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [drag_and_drop_state.schedule])

    const targets = drag_and_drop_state.schedule.map( (_, index)=> 
        <Target key={index} id={`${index}`} state={drag_and_drop_state.schedule}>
            <p>Simultaneous Items</p> 
        </Target>    
    ) 

    return (
        <div>
            <h3>Plan Schedule Management</h3>
            <p>
                Slide 60 from section 2.  Create a network diagram
            </p>

            <div className="grid grid-cols-2 gap-x-4">
                <Droppable droppableId="from"  >
                    {(provided, snapshot) => {
                        return (
                            <div
                                {...provided.droppableProps}
                                ref={provided.innerRef}
                            >
                                <MyList data={drag_and_drop_state.from} />
                            </div>
                        )
                    }}
                </Droppable>

                <div className="flex flex-col">
                    { targets }
                    <Target id="new" state={drag_and_drop_state} color="gray">
                        <p className="mb-20"> Consecutive items </p>
                    </Target>

                </div>
            </div>
            {/* </DragDropContext> */}
        </div>
    )
}

export default Schedule
