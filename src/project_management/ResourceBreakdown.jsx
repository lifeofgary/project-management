import { H3 } from "../components/Components"
import TableForProject from "./TableForProject"
import { 
    EDIT_RESOURCE_BREAKDOWN,
    ADD_RESOURCE_BREAKDOWN,
  } from "./reducer";
  
const ResourceBreakdown = props => {
    return (
        <>
            <H3>Resource Breakdown Structure</H3>
            <p className="text-gray-400">
                Slide 79 from section 2
            </p>

            <TableForProject {...props} 
                on_edit_action={EDIT_RESOURCE_BREAKDOWN}
                on_add_action={ADD_RESOURCE_BREAKDOWN}            
            />
        </>
    )
}

export default ResourceBreakdown