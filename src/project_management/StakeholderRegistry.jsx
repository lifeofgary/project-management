
import RaciLegend from "./RaciLegend";
import { H3 } from "../components/Components";
import TableForProject from "./TableForProject";
import { 
    EDIT_STAKEHOLDER_REGISTRY,
    ADD_STAKEHOLDER_REGISTRY,
  } from "./reducer";
import ExampleButton from "../components/ExampleButton";
import Example from "./examples/StakeholderRegistryExample"
  
const StakeholderRegistry = ({...props})=>{
    return (
        <>
            <H3>Stakeholder Registry</H3>
            <TableForProject 
                {...props} 
                on_edit_action={EDIT_STAKEHOLDER_REGISTRY}
                on_add_action={ADD_STAKEHOLDER_REGISTRY}
            />
            <RaciLegend/>
            <ExampleButton>
                <Example />
            </ExampleButton>
        </>        
    )
}

export default StakeholderRegistry;