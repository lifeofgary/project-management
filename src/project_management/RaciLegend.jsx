
import { H4 } from "../components/Components"
import { legend } from "../Classes"

const RaciLegend = ()=> {

    let header_class = legend.header;
    let odd_row  = legend.odd_row;
    let even_row = legend.even_row

    return (
        <>
            <H4 className="text-gray-500">Legend</H4>

            <div className={legend.grid}>
                <div className={header_class}>ROLE</div>
                <div className={header_class}>DEFINED</div>

                <div className={odd_row}>R</div>
                <div className={odd_row}>Responsible for doing work on the project</div>

                <div className={even_row}>A</div>
                <div className={even_row}>Accountable for project outcome</div>

                <div className={odd_row}>C</div>
                <div className={odd_row}>Consult as a Subject Matter Export (SME)</div>

                <div className={even_row}>I</div>
                <div className={even_row}>Inform as the project progresses</div>

            </div>
    
        </>
    )
}

export default RaciLegend