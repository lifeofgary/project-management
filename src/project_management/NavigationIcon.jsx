import { Link } from "react-router-dom";

import * as Icon  from 'react-icons/fa';


// rotate-45
const Arrow = ()=> {
    return (
        <Icon.FaArrowRight className="inline my-4 ml-1"/>
    )
}

const NavigationIcon = ({to="/", image, active, text, no_example, in_progress, end }) => {
    // const image = "arrow";
    // "brightness-50" "sepia" "grayscale"
    let text_color = "";
    if (no_example) text_color="text-cyan-600";
    if (in_progress) text_color="text-red-600";

    const filter = active ? "" : "sepia";
    const bold = active ? "font-semibold" : "";
    const size = "w-10 h-auto"

    let t = text.split(" ")
    let t2 = t.map( (a, b) => <span key={a}>{a}<br/></span> )

    return (
        <>
            <div className="flex flex-row ">
                <Link to={to}>
                    <div className="flex flex-col">
                    <img src={`/project-management/icons/${image}.svg`} className={`${filter} ${size} inline mx-1 cursor-pointer`} alt=""/>
                    <span className={`${text_color} ${bold}` }>{t2}</span>
                    </div>
                </Link>
                { end ? '' : <Arrow /> }
            </div>
        </>
    )
}

export default NavigationIcon
