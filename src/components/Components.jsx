
const _class = (first, second)=> (!second) ? first : first + " " + second.className;

let color = "teal"

export const H1 = props => <h1 className={_class(`text-4xl text-${color}-800`, props)}> {props.children}</h1>
export const H2 = props => <h2 className={_class(`text-3xl text-${color}-700`, props)}> {props.children}</h2>
export const H3 = props => <h3 className={_class(`text-2xl text-${color}-600`, props)}> {props.children}</h3>
export const H4 = props => <h4 className={_class(`text-xl text-${color}-500`, props)}> {props.children}</h4>
export const H5 = props => <h5 className={_class(`text-g text-${color}-400`, props)}> {props.children}</h5>


