
// https://v1.tailwindcss.com/components/alerts

const TealButton = ({children, ...props})=>{

    let {color, transparent, border, disabled, broken} = props

    let weight=500;
    let text_color = "text-white"
    let lite_gray=false;

    if (color==="dark_gray") {
        color="zinc";
        weight=600
    }
    if (color==="gray") {
        color="slate";
    }
    if (color==="lite_gray") {
        color="gray";
        weight=300
        lite_gray = true;
    }

    let bold_weight = weight+200;

    if (broken==='teal')  { return <AbstractButton className="text-teal-500  hover:bg-teal-500  border-teal-500  hover:bg-teal-700"/> }
    if (broken==='green') { return <AbstractButton className="text-green-500 hover:bg-green-500 border-green-500 hover:bg-green-700"/> }
    if (broken==='cyan')  { return <AbstractButton className="text-cyan-500  hover:bg-cyan-500  border-cyan-500  hover:bg-cyan-700"/> }
    if (broken==='purple')  { return <AbstractButton className="text-purple-500  hover:bg-purple-500  border-purple-500  hover:bg-purple-700"/> }
    if (broken==='amber')  { return <AbstractButton className="text-amber-500  hover:bg-amber-500  border-amber-500  hover:bg-amber-700"/> }
    if (broken==='orange')  { return <AbstractButton className="text-orange-500  hover:bg-orange-500  border-orange-500  hover:bg-orange-700"/> }
    if (broken==='red')  { return <AbstractButton className="text-red-500  hover:bg-red-500  border-red-500  hover:bg-red-700"/> }
    if (broken==='gray')  { return <AbstractButton className="text-gray-300  hover:bg-gray-300  border-gray-300  hover:bg-gray-500"/> }
    if (broken==='light-gray')  { return <AbstractButton className="text-slate-500  hover:bg-slate-500  border-slate-500  hover:bg-slate-700"/> }
    if (broken==='dark-gray')  { return <AbstractButton className="text-zinc-600  hover:bg-zinc-600  border-zinc-600  hover:bg-zinc-800"/> }

    let classes = []

    if (transparent) {
        classes.push(lite_gray ? "text-gray-300" : `text-${color}-${weight}`)
        classes.push(`hover:bg-${color}-${weight}`)
        classes.push(`hover:text-${color}-${weight}`)
        classes.push("font-semibold")
        classes.push(`hover:${text_color}`);
        classes.push("hover:border-transparent");
    } else if (border) {
        classes.push(lite_gray ? "text-gray-300" : `text-${color}-${weight}`)
        classes.push(`hover:bg-${color}-${weight}`)
        classes.push(`hover:text-white`)
        classes.push(`border-${color}-${weight}`)
        classes.push("border")
    } else {
        classes.push(`bg-${color}-${weight}`)
        classes.push(`hover:bg-${color}-${bold_weight}`)
        classes.push(text_color)
        classes.push("hover:border-transparent")
    }

    if (disabled) {
        classes.push("opacity-50")
        classes.push("cursor-not-allowed")
    }

    return <AbstractButton className={`${classes.join(' ')}`} {...props}>
        {children}
    </AbstractButton>
}

export const button_style = (color = 'teal', options={}) => {
    let {disabled, border} = options
    if (options.disabled) {
        disabled = "opacity-50 cursor-not-allowed"
    }

    let weight = 500
    let dark = 500 + 200
    if (border)
        return `${disabled} rounded-xl py-2 px-4 m-1 text-${color}-${weight}  hover:bg-${color}-${weight} hover:text-white border border-${color}-${weight}  hover:bg-${color}-${dark}`


    return `${disabled} rounded-xl py-2 px-4 m-1 bg-${color}-${weight} text-white  hover:bg-${color}-${weight}  border-${color}-${weight}  hover:bg-${color}-${dark}`
}

const AbstractButton = ({size="base", color, border, transparent, pill, rectangle, className, children, ...props}) => {
    let py=2, px=4, margin=1, font_size = `text-${size}`
    let shape="rounded-xl"

    if (size==="xs") {
        px--;
        px--;
        py--;
    }
    if (size==="sm") {
        px--;
        py--;
    }
    if (size==="4xl") {
        px++;
        py++;
    }

    let dimensions = `py-${py} px-${px} m-${margin} ${font_size}`

    if (pill)
        shape = "rounded-full"
    else if (rectangle)
        shape = "outline"

    return (
        <button className={`${color} ${shape} ${dimensions} ${className}`} {...props}>
            {children}
        </button>
    )
}

const Button = ({
        color="cyan",
        children,
        teal,
        green,
        cyan,
        purple,
        amber,
        orange,
        red,
        dark_gray,
        lite_gray,
        gray,
        ...props
}) => {
    if (teal)       color = 'teal'
    if (green)      color = 'green'
    if (cyan)       color = 'cyan'
    if (purple)     color = 'purple'
    if (amber)      color = 'amber'
    if (orange)     color = 'orange'
    if (red)        color = 'red'
    if (dark_gray)  color = 'dark_gray'
    if (lite_gray)  color = 'lite_gray'
    if (gray)       color = 'gray'

    return <TealButton  color={color} {...props}> {children} </TealButton>
}

export default Button
