import { useRef } from "react";
import ContentEditable from "react-contenteditable";

const EditibleCell = ({onEdit, styles, text}) => {
    const ref = useRef(text)
    const handleChange = evt => {
        let {value} = evt.target;
        ref.current = value
        onEdit(value);
    }
    return (
        <ContentEditable
            className={styles} 
            html={ref.current}
            onChange={handleChange}
        />
    )
}

export default EditibleCell