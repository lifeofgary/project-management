
import {useState} from "react"
import Button from "./Button"
import {H4} from "../components/Components"

let ExampleButton = ({children})=> {
    let [show_example, set_show_example] = useState(false)

    return (
        <>
            <p>
                <Button onClick={()=>set_show_example(!show_example)}>
                    { show_example ? "Hide Example" : "Show Example"}
                </Button>
            </p>
            { show_example ? (<><H4>Example</H4>{children}</>) : '' }
        </>
    )
} 

export default ExampleButton
