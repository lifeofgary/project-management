
const Temp = ()=>{
    return (
        <>
      <h2 className="text-3xl font-bold">
        Initiate
      </h2>
      <Menu>
        <Menu.Item>
          <a>Project Charter</a>
        </Menu.Item>
        <Menu.Item>
          <a>Stakeholder Registry</a>
        </Menu.Item>
      </Menu>

      <h2 className="text-3xl font-bold">
        Plan
      </h2>
      <Menu>
        <Menu.Item>
          <a>Schedule baseline</a>
        </Menu.Item>
        <Menu.Item>
          <a>Cost baseline</a>
        </Menu.Item>
        <Menu.Item>
          <a>Quality Management Plan</a>
        </Menu.Item>
        <Menu.Item>
          <a>Resource Management Plan</a>
        </Menu.Item>
        <Menu.Item>
          <a>Communications Management Plan</a>
        </Menu.Item>

        <Menu.Item>
          <a>Risk Management Plan</a>
        </Menu.Item>
        <Menu.Item>
          <a>Procurement Management Plan</a>
        </Menu.Item>
        <Menu.Item>
          <a>Risk Registry</a>
        </Menu.Item>
        <Menu.Item>
          <a>Team Charter</a>
        </Menu.Item>
      </Menu>



		{/* <button className="btn">
  			Get Started
  		</button> */}


		<ul className="list-disc">
			<li className="list-decimal">./Initiating/Part 3/3a.  Portfolio Planning.xlsx</li>
			<li>./Initiating/Part 3/6. Part 3 Benefits Mgmt Plan Example.xlsx</li>
			<li>./Initiating/Part 3/7. Part 3 Benefits Mgm Plan Template.xlsx</li>
			<li>./Initiating/Part 4/4a. Part 4 Activity 2 Stakeholder Reg Example.xlsx</li>
			<li>./Initiating/Part 4/4b. Part 4 Activity 2 Stakeholder Reg Temp.xlsx</li>
			<li>./Initiating/Part 4/5. Part 4 Activity 3 Assump Log.xlsx</li>

			<li>./Planning/Part 1/5. Part 1 Activity 3 Stakeholder Eng Plan.xlsx</li>
			<li>./Planning/Part 2/4. Part 2 Activity 1 CTQ Tree.xlsx</li>
			<li>./Planning/Part 2/14. Part 2 Activity 5 RBS.xlsx</li>
			<li>./Planning/Part 3/2. Part 3 Activity 1 Quality Mgmt Plan.xlsx</li>
			<li>./Planning/Part 3/15. Part 3 Activity 7 Risk Reg Template.xlsx</li>
		</ul>

		<ol>
			<li>Initiating/Part 1/1. Part 1 Activity 1.docx </li>
			<li>Initiating/Part 1/2. Part 1 Activity 2.docx </li>
			<li>Initiating/Part 1/3. Part 1 Activity 3.docx </li>
			<li>Initiating/Part 1/4. Part 1 Skill Check.docx </li>
			<li>Initiating/Part 1/5. Part 1 Retrospective.docx </li>
			<li>Initiating/Part 2/1. Part 2 PMI Framework.docx </li>
			<li>Initiating/Part 2/10. Part 2 Skill Check 4 - M and C.docx </li>
			<li>Initiating/Part 2/11. Part 2 Close the Project Process Group.docx </li>
			<li>Initiating/Part 2/12. Part 2 Skill Check 5 Closing.docx </li>
			<li>Initiating/Part 2/13. Part 2 Activity 1.docx </li>
			<li>Initiating/Part 2/14. Part 2 Retrospective.docx </li>
			<li>Initiating/Part 2/2. Part 2 PMI Lifecycle Attributes.docx </li>
			<li>Initiating/Part 2/3. Part 2 Initiating Process Group.docx </li>
			<li>Initiating/Part 2/4. Part 2 Skill Check 1 Initiating.docx </li>
			<li>Initiating/Part 2/5. Part 2 Planning Process Group.docx </li>
			<li>Initiating/Part 2/6. Part 2 Skill Check 2 Planning.docx </li>
			<li>Initiating/Part 2/7. Part 2 Executing Process Group.docx </li>
			<li>Initiating/Part 2/8. Part 2 Skill Check 3 Executing.docx </li>
			<li>Initiating/Part 2/9. Part 2 Monitor and Control Process Group.docx </li>
			<li>Initiating/Part 3/1. Part 3 A Portfolio Approach.docx </li>
			<li>Initiating/Part 3/10. Part 3 Skill Check.docx </li>
			<li>Initiating/Part 3/11. Part 3 Retrospective.docx </li>
			<li>Initiating/Part 3/2. Part 3 A Portfolio Approach Example.docx </li>
			<li>Initiating/Part 3/3. Part 3 Activity 1 Portfolio.docx </li>
			<li>Initiating/Part 3/4. Part 3 The PMO Fit.docx </li>
			<li>Initiating/Part 3/5. Part 3 Activity 2 PMO.docx </li>
			<li>Initiating/Part 3/8. Part 3 Activity 3 Business Case Format 2021.docx </li>
			<li>Initiating/Part 3/9. Part 3 Business Case Example 2021.docx </li>
			<li>Initiating/Part 4/1. Part 4 Initiating Process Group.docx </li>
			<li>Initiating/Part 4/2. Part 4 Activity 1 Project Charter Format.docx </li>
			<li>Initiating/Part 4/2a. Part 4 Activity 1 Project Charter Example.docx </li>
			<li>Initiating/Part 4/3. Part 4 Activity 1 Project Charter Template.docx </li>
			<li>Initiating/Part 4/6. Part 4 Retrospective.docx </li>
			<li>Initiating/Part 4/7. Part 4 Skill Check.docx </li>
		</ol>

		<ol>
			<li> Planning/Part 1/1. Part 1 Planning Process Sequence 2021.docx </li>
			<li> Planning/Part 1/2. Part 1 Activity 1.docx </li>
			<li> Planning/Part 1/3. Part 1 Activity 2.docx </li>
			<li> Planning/Part 1/4. Part 1 Activity 3 Project Charter Case Study 2021.docx </li>
			<li> Planning/Part 1/5. Part 1 Skill Check.docx </li>
			<li> Planning/Part 1/6. Part 1 Retrospective.docx </li>
			<li> Planning/Part 2/1. Part 2 Project Scope Management 2021.docx </li>
			<li> Planning/Part 2/10. Part 2 Network Diagramming Steps.docx </li>
			<li> Planning/Part 2/11. Part 2 Activity 4 Network Diagram.docx </li>
			<li> Planning/Part 2/12. Part 2 Schedule Planning Skill Check.docx </li> 
			<li> Planning/Part 2/13. Part 2 Project Cost Management 2021.docx </li>
			<li> Planning/Part 2/15. Part 2 Retrospective.docx </li> 
			<li> Planning/Part 2/2. Part 2 Requirement Types to Consider.docx </li>
			<li> Planning/Part 2/3. Part 2 Activity 1 Catagorized Req.docx </li>
			<li> Planning/Part 2/6. Part 2 Activity 3 WBS.docx </li>
			<li> Planning/Part 2/7. Part 2 Activity 3 WBS Dictionary.docx </li>
			<li> Planning/Part 2/8. Part 2 Scope Planning Skill Check.docx </li>
			<li> Planning/Part 2/9. Part 2 Project Sched Mgmt 2021.docx </li>
			<li> Planning/Part 3/1. Part 3 Project Quality Management Plan 2021.docx </li>
			<li> Planning/Part 3/3. Part 3 Project Resource Management 2021.docx </li>
			<li> Planning/Part 3/4. Part 3 Activity 2 RAM Example.docx </li>
			<li> Planning/Part 3/5. Part 3 Activity 2 RAM Template.docx </li>
			<li> Planning/Part 3/6. Part 3 Activity 3 Team Charter Template.docx </li>
			<li> Planning/Part 3/7. Part 3 Skill Check 1.docx </li>
			<li> Planning/Part 3/8. Part 3 Communications Management Plan 2021.docx </li>
			<li> Planning/Part 3/9. Part 3 Activity 4 Comm Mgm Plan Example.docx </li>
			<li> Planning/Part 3/10. Part 3 Activity 4 Comm Mgm Plan Template.docx </li>
			<li> Planning/Part 3/11. Part 3 Activity 4 Comm Mgm Plan Plus Template.docx </li>
			<li> Planning/Part 3/12. Part 3 Activity 5 Project Status Report Template.docx </li>
			<li> Planning/Part 3/13. Part 3 Project Risk Management 2021.docx </li>
			<li> Planning/Part 3/13b. Part 3 Project Risk Management Example.docx </li>
			<li> Planning/Part 3/14. Part 3 Activity 6 Risk Management Plan.docx </li>
			<li> Planning/Part 3/16. Part 3 Skill Check 2.docx </li>
			<li> Planning/Part 3/17. Part 3 Project Procurement Management 2021.docx </li>
			<li> Planning/Part 3/18. Part 3 Activity 8 Procurement Analysis.docx </li>
			<li> Planning/Part 3/18. Part 3 Retrospective.docx </li>
			<li> Planning/Part 4/1. Part 4 Project Management Plan Approval 2021.docx </li>
			<li> Planning/Part 4/2. Part 4 Activity 1 Project Pitch.docx </li>
			<li> Planning/Part 4/3. Part 4 Activity 2 Planning Events.docx </li>
			<li> Planning/Part 4/4. Part 4 Skill Check.docx </li>
			<li> Planning/Part 4/5. Part 4 Retrospective.docx </li>
		</ol>        
        </>
    )
}

export default Temp