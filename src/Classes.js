
export const legend = {
    title: "text-gray-600 text-xl",
    grid: "grid grid-cols-2 min-w-fill",
    header: "font-bold bg-gray-300 text-gray-500",
    odd_row: "bg-gray-100 text-gray-500",
    even_row: "bg-gray-200 text-gray-500",
}

export const table = {
    grid: size => `grid grid-cols-${size} min-w-fill gap-x-0.5 `,
    header: color => `font-bold bg-${color}-300 text-gray-500`,
    odd_row: color => `bg-${color}-100 text-gray-500`,
    even_row: color => `bg-${color}-200 text-gray-500`,
}

export const add_class = (classes, item) => {
    classes = classes.split(' ')
    return classes.concat(item)
}
